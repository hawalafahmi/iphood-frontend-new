import { ADD_FILIAL_SUCCESS, ADD_FILIAL_CLEAR, UPDATE_FILIAL_SUCCESS, USER_FILIALS_LOAD, ADD_FILIAL_CARD } from '../type'

const initialState = {
       restaurant: null,
       action: null,
       updateedFilial: null,
       card: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case USER_FILIALS_LOAD:
                     return {
                            ...state,
                            currentFilials: payload,
                     }
              case ADD_FILIAL_SUCCESS:
                     return {
                            ...state,
                            restaurant: payload,
                            action: 'ADD_FILIAL_CLEAR',
                     }
              case UPDATE_FILIAL_SUCCESS:
                     return {
                            ...state,
                            updateedFilial: payload,
                            action: 'UPDATE_FILIAL_SUCCESS',
                     }
              case ADD_FILIAL_CARD:
                     return {
                            ...state,
                            card: payload.card,
                            action: 'ADD_FILIAL_CARD',
                     }
              case ADD_FILIAL_CLEAR:
                     return {
                            ...state,
                            action: null,
                     }

              default:
                     return state
       }
}
