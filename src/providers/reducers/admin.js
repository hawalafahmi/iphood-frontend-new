import {
    ADMIN_GET_ALL_SUCCESS,
    ADMIN_GET_ID_SUCCESS,
    ADMIN_GET_ID_CLEAR,
    ADMIN_UPDATE_SUCCESS,
    ADMIN_UPDATE_CLEAR,
    USER_LOGOUT,
} from '../type'

const initialState = {
    list: null,
    oneAdmin: null,
    updateAdmin: null,
    action: false,
}

export default function (state = initialState, action) {
    const { type, payload } = action

    switch (type) {
        case ADMIN_GET_ALL_SUCCESS:
            return {
                ...state,
                list: payload,
            }
        case ADMIN_GET_ID_SUCCESS:
            return {
                ...state,
                oneAdmin: payload,
            }
        case ADMIN_GET_ID_CLEAR:
            return {
                ...state,
                oneAdmin: null,
            }
        case ADMIN_UPDATE_SUCCESS:
            return {
                ...state,
                updateAdmin: payload,
                action: true,
            }
        case ADMIN_UPDATE_CLEAR:
            return {
                ...state,
                updateAdmin: null,
                action: false,
            }
        case USER_LOGOUT:
            return {
                list: null,
                oneAdmin: null,
                updateAdmin: null,
                action: false,
            }

        default:
            return state
    }
}
