import { combineReducers } from 'redux'
import error from './error'
import auth from './auth'
import admin from './admin'
import restaurant from './restaurant'
import filial from './filial'
import card from './card'
import category from './category'
import product from './product'
import menu from './menu'
import ingredient from './ingredient'

export default combineReducers({
       errors: error,
       auth: auth,
       admin: admin,
       restaurant: restaurant,
       filial: filial,
       card: card,
       category: category,
       ingredient: ingredient,
       menu: menu,
       product: product,
})
