import {
       ADD_CATEGORY_SUCCESS,
       GET_ERRORS,
       CLEAR_ERRORS,
       GET_CATEGORY_SUCCESS,
       GET_CATEGORY_CLEAR,
       ADD_CATEGORY_CLEAR,
       ADD_CATEGORY_REFRESH,
} from '../type'

const initialState = {
       card: null,
       category: null,
       action: null,
       updateedFilial: null,
       refresh: false,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case ADD_CATEGORY_SUCCESS:
                     return {
                            ...state,
                            card: payload,
                            category: payload.category,
                            action: 'ADD_CATEGORY_SUCCESS',
                            refresh: true,
                     }

              case ADD_CATEGORY_CLEAR:
                     return {
                            ...state,
                            action: null,
                     }
              case ADD_CATEGORY_REFRESH:
                     return {
                            ...state,
                            refresh: false,
                     }

              default:
                     return state
       }
}
