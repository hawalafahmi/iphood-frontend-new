import { ADD_REST_SUCCESS, ADD_REST_CLEAR } from '../type'

const initialState = {
       newUser: null,
       action: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case ADD_REST_SUCCESS:
                     return {
                            ...state,
                            newUser: payload,
                            action: 'REST_REGUSTER_SUCCESS',
                     }
              case ADD_REST_CLEAR:
                     return {
                            ...state,
                            newUser: null,
                            action: null,
                     }

              default:
                     return state
       }
}
