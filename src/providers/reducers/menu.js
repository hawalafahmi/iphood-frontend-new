import { ADD_MENU_SUCCESS, ADD_MENU_REFRESH, GET_MENU_SUCCESS, ADD_MENU_CATEGORY_SUCCESS, GET_ONE_MENU_CLEAR } from '../type'

const initialState = {
       action: null,
       newMenu: null,
       list: null,
       category: null,
       currentMenu: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case ADD_MENU_SUCCESS:
                     return {
                            ...state,
                            newMenu: payload,
                            action: 'ADD_MENU_SUCCESS',
                     }
              case GET_MENU_SUCCESS:
                     return {
                            ...state,
                            list: payload,
                            action: 'GET_MENU_SUCCESS',
                     }
              case GET_ONE_MENU_CLEAR:
                     return {
                            ...state,
                            currentMenu: payload,
                            action: 'GET_ONE_MENU_CLEAR',
                     }
              case ADD_MENU_CATEGORY_SUCCESS:
                     return {
                            ...state,
                            category: payload,
                            action: 'ADD_MENU_CATEGORY_SUCCESS',
                     }
              case ADD_MENU_REFRESH:
                     return {
                            ...state,
                            action: null,
                     }

              default:
                     return state
       }
}
