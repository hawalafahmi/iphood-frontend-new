import { REGISTER_SUCCESS, USER_LOGIN_SUCCESS, USER_LOADED, AUTH_ERROR, USER_LOGOUT, CLEAR_REGISTER } from '../type'

const initialState = {
       token: localStorage.getItem('token'),
       user: null,
       isAuthenticated: false,
       loading: true,
       register: null,
       newUser: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case USER_LOADED:
                     return {
                            ...state,
                            isAuthenticated: true,
                            loading: false,
                            user: payload,
                     }
              case REGISTER_SUCCESS:
                     return {
                            ...state,
                            newUser: payload,
                            register: 'USER_REGUSTER_SUCCESS',
                     }
              case CLEAR_REGISTER:
                     return {
                            ...state,
                            register: null,
                     }
              case USER_LOGIN_SUCCESS:
                     localStorage.setItem('token', payload.Token)
                     return {
                            ...state,
                            ...payload,
                            isAuthenticated: true,
                            token: payload.Token,
                     }
              case AUTH_ERROR:
              case USER_LOGOUT:
                     localStorage.removeItem('token')
                     return {
                            user: null,
                            isAuthenticated: false,
                            loading: true,
                            register: null,
                            newUser: null,
                     }

              default:
                     return state
       }
}
