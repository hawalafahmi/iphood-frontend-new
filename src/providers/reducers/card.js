import { ADD_CARD_SUCCESS, ADD_CARD_CLEAR, GET_CARD_SUCCESS, GET_CARD_CLEAR, USER_CARDS_LOAD } from '../type'

const initialState = {
       action: null,
       updateCard: null,
       card: null,
}

export default function (state = initialState, action) {
       const { type, payload } = action

       switch (type) {
              case USER_CARDS_LOAD:
                     return {
                            ...state,
                            currentCards: payload,
                     }
              case ADD_CARD_SUCCESS:
                     return {
                            ...state,
                            updateCard: payload,
                            action: 'ADD_CARD_SUCCESS',
                     }
              case GET_CARD_SUCCESS:
                     return {
                            ...state,
                            card: payload,
                     }
              case ADD_CARD_CLEAR:
                     return {
                            ...state,
                            action: null,
                     }
              case GET_CARD_CLEAR:
                     return {
                            ...state,
                            card: null,
                     }

              default:
                     return state
       }
}
