import { GET_ERRORS, CLEAR_ERRORS, USER_LOGOUT } from '../type';

const initialState = {};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ERRORS:
      return action.payload;
    case CLEAR_ERRORS:
    case USER_LOGOUT:
      return {};

    default:
      return state;
  }
}
