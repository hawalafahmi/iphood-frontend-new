import { ADD_PRODUCT_SUCCESS, CLEAR_ERRORS, GET_ERRORS, GET_PRODUCT_SUCCESS } from '../type'
import API from '../../utils/API'
import { getOneCardByName } from './card'

export const addNewProduct = (categoryID, data) => async (dispatch) => {
       try {
              const res = await API.post('/product/create/category/' + categoryID, data, {
                     headers: { 'Content-Type': 'multipart/form-data' },
              })
              dispatch({ type: CLEAR_ERRORS })
              dispatch({
                     type: ADD_PRODUCT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const createUnRelatedProduct = (data) => async (dispatch) => {
       try {
              const res = await API.post('/product/create', data, {
                     headers: { 'Content-Type': 'multipart/form-data' },
              })
              dispatch(getAllProduct())
              dispatch({ type: CLEAR_ERRORS })
              dispatch({
                     type: ADD_PRODUCT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const addExistingProduct = (CategoryId, data) => async (dispatch) => {
       try {
              const res = await API.post('/product/add/' + CategoryId, data)
              dispatch({
                     type: ADD_PRODUCT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const getAllProduct = () => async (dispatch) => {
       try {
              const res = await API.get('/product/')
              dispatch({
                     type: GET_PRODUCT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const removeProductFromCategory = (cardID, categoryID, productID) => async (dispatch) => {
       try {
              const res = await API.put(`/category/product/${categoryID}/${productID}`)
              dispatch(getOneCardByName(cardID))
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const deleteProduct = (productID) => async (dispatch) => {
       try {
              const res = await API.delete(`/product/${productID}`)
              dispatch(getAllProduct())
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const editProduct = (productID, data) => async (dispatch) => {
       try {
              console.log('eziajezpoaezaeza', data)
              const res = await API.put(`/product/${productID}`, data)
              dispatch(getAllProduct())
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
