import {
       ADD_INGREDIENT_SUCCESS,
       GET_INGREDIENT_SUCCESS,
       CLEAR_ERRORS,
       GET_ERRORS,
       EDIT_INGREDIENT_SUCCESS,
       REMOVE_INGREDIENT_SUCCESS,
       ADD_PRODUCT_SUCCESS,
} from '../type'
import API from '../../utils/API'
import { getAllProduct } from './product'
export const addNewIngredient = (data) => async (dispatch) => {
       try {
              const res = await API.post('/ingredient/', data)
              dispatch({
                     type: CLEAR_ERRORS,
              })
              dispatch(getAllIngredients())
              dispatch({
                     type: ADD_INGREDIENT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const EditIngredient = (ingID, data) => async (dispatch) => {
       try {
              const res = await API.put('/ingredient/' + ingID, data)
              dispatch({
                     type: CLEAR_ERRORS,
              })
              dispatch(getAllIngredients())
              dispatch({
                     type: EDIT_INGREDIENT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const removeIngredient = (id) => async (dispatch) => {
       try {
              const res = await API.delete('/ingredient/' + id)
              dispatch(getAllIngredients())
              dispatch({
                     type: REMOVE_INGREDIENT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const getAllIngredients = () => async (dispatch) => {
       try {
              const res = await API.get('/ingredient')
              dispatch({
                     type: GET_INGREDIENT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const assignExistingIngredient = (productID, data) => async (dispatch) => {
       try {
              const res = await API.post('/product/inregient/add/' + productID, data)
              dispatch(getAllProduct())
              dispatch({
                     type: ADD_PRODUCT_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const removeIngredientFromProduct = (ingredientID, productID, data) => async (dispatch) => {
       try {
              const res = await API.put('/ingredient/product/' + ingredientID + '/' + productID, data)
              dispatch(getAllProduct())
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
