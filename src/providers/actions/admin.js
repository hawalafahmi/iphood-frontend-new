import {
  ADMIN_GET_ALL_SUCCESS,
  ADMIN_GET_ALL_FAIL,
  ADMIN_GET_ID_SUCCESS,
  ADMIN_GET_ID_FAIL,
  ADMIN_UPDATE_SUCCESS,
  GET_ERRORS,
} from '../type';
import API from '../../utils/API';

export const getAllAdmins = () => async dispatch => {
  try {
    const res = await API.get('/admin/all');
    dispatch({
      type: ADMIN_GET_ALL_SUCCESS,
      payload: res.data.data,
    });
  } catch (_err) {
    dispatch({
      type: ADMIN_GET_ALL_FAIL,
      payload: null,
    });
  }
};
export const getAdminById = userID => async dispatch => {
  try {
    const res = await API.get('/admin/details/' + userID);
    dispatch({
      type: ADMIN_GET_ID_SUCCESS,
      payload: res.data.data,
    });
  } catch (_err) {
    dispatch({
      type: ADMIN_GET_ID_FAIL,
      payload: null,
    });
  }
};
export const updateAdminByID = (userID, data) => async dispatch => {
  try {
    const res = await API.put('/admin/' + userID, data);
    dispatch({
      type: ADMIN_UPDATE_SUCCESS,
      payload: res.data.data,
    });
    dispatch(getAllAdmins())
  } catch (_err) {
    const errors = _err.response.data.error;
    dispatch({
      type: GET_ERRORS,
      payload: errors,
    });
  }
};
