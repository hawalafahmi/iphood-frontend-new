import {
  REGISTER_SUCCESS,
  GET_ERRORS,
  USER_LOGIN_SUCCESS,
  USER_LOADED,
  AUTH_ERROR,
  USER_LOGOUT,
  USER_CARDS_LOAD,
  USER_FILIALS_LOAD,
} from "../type";
import { clearErrors } from "./error";
import API from "../../utils/API";
export const loadUser = () => async (dispatch) => {
  try {
    const response = await API.get("/user/user");
    dispatch({
      type: USER_LOADED,
      payload: response.data.data,
    });
    if (response.data.data.isAdmin) {
      dispatch({
        type: USER_FILIALS_LOAD,
        payload: response.data.data.restaurant[0].filials,
      });
      dispatch({
        type: USER_CARDS_LOAD,
        payload: response.data.data.restaurant[0].card,
      });
    }
  } catch (error) {
    dispatch({
      type: AUTH_ERROR,
    });
  }
};
export const register = (data) => async (dispatch) => {
  dispatch(clearErrors());
  try {
    const res = await API.post("/auth/register", data);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data.data,
    });
    /* dispatch(loadUser()) */
    /* dispatch(getAllAdmins()) */
  } catch (_err) {
    const errors = _err.response.data.error;
    dispatch({
      type: GET_ERRORS,
      payload: errors,
    });
  }
};

export const login = (data) => async (dispatch) => {
  dispatch(clearErrors());
  try {
    const res = await API.post("/auth/login", data);
    dispatch({
      type: USER_LOGIN_SUCCESS,
      payload: res.data.data,
    });
    dispatch(loadUser());
  } catch (_err) {
    const errors = _err.response.data.error;
    dispatch({
      type: GET_ERRORS,
      payload: errors,
    });
  }
};
export const logout = () => (dispatch) => {
  dispatch({ type: USER_LOGOUT });
};
