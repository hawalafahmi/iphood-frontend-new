import { ADD_CATEGORY_SUCCESS, GET_ERRORS, CLEAR_ERRORS, GET_CATEGORY_SUCCESS, GET_CATEGORY_CLEAR } from '../type'
import API from '../../utils/API'
import { loadUser } from './auth'

export const addNewCategory = (cardName, data) => async (dispatch) => {
       try {
              const res = await API.post('/category/' + cardName, data, {
                     headers: { 'Content-Type': 'multipart/form-data' },
              })
              dispatch({ type: CLEAR_ERRORS })
              dispatch({
                     type: ADD_CATEGORY_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
