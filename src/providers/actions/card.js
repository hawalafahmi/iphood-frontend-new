import { ADD_CARD_SUCCESS, GET_ERRORS, GET_CARD_SUCCESS, GET_CARD_CLEAR } from '../type'
import API from '../../utils/API'
import { loadUser } from './auth'

export const addNewCard = (data) => async (dispatch) => {
       try {
              const res = await API.post('/card', data)
              dispatch({
                     type: ADD_CARD_SUCCESS,
                     payload: res.data.data,
              })
              dispatch(loadUser())
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}

export const getOneCardByName = (cardID) => async (dispatch) => {
       try {
              const res = await API.get('/Card/' + cardID)

              dispatch({
                     type: GET_CARD_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
