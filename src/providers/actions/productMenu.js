import { EDIT_PRODUCT_MENU, REFRECH_PRODUCT_MENU, REMOVE_PRODUCT_MENU, GET_ERRORS } from '../type'
import API from '../../utils/API'
import { getMenuBuID } from './menu'

export const removeProductFromMenu = (menuID, categoryID, productID) => async (dispatch) => {
       try {
              const res = await API.delete('/menu/category/product/' + categoryID + '/' + productID)
              dispatch(getMenuBuID(menuID))
              dispatch({
                     type: REMOVE_PRODUCT_MENU,
                     payload: res,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const editProductMenu = (menuID, productID, data) => async (dispatch) => {
       try {
              const res = await API.put('/menu/product/' + productID, data, {
                     headers: { 'Content-Type': 'multipart/form-data' },
              })
              dispatch(getMenuBuID(menuID))
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
