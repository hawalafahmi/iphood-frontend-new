import { ADD_MENU_SUCCESS, GET_ERRORS, GET_MENU_SUCCESS, ADD_MENU_CATEGORY_SUCCESS, GET_ONE_MENU_CLEAR } from '../type'
import API from '../../utils/API'
import { loadUser } from './auth'
export const addNewMenu = (data) => async (dispatch) => {
       try {
              const res = await API.post('/menu', data)
              dispatch(loadUser())
              dispatch({
                     type: ADD_MENU_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const getListMenuByCardID = (cardID) => async (dispatch) => {
       try {
              const res = await API.post('/menu/card' + cardID)
              dispatch(loadUser())
              dispatch({
                     type: GET_MENU_SUCCESS,
                     payload: res.data.data.menuCategory,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const addExistingCategoryToMenu = (menuID, data) => async (dispatch) => {
       try {
              const res = await API.post('/menu/add/' + menuID, data)
              dispatch(getMenuBuID(res.data.data._id))

              dispatch({
                     type: ADD_MENU_CATEGORY_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const getMenuBuID = (menuID) => async (dispatch) => {
       try {
              const res = await API.get('/menu/' + menuID)
              dispatch({
                     type: GET_ONE_MENU_CLEAR,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
