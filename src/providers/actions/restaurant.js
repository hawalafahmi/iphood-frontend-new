import { ADD_REST_SUCCESS, GET_ERRORS, CLEAR_ERRORS } from '../type'
import API from '../../utils/API'
import { getAllAdmins } from './admin'
export const addNewRestaurant = (userID, data) => async (dispatch) => {
    try {
        const res = await API.post('/restaurant/' + userID, data, {
            headers: { 'Content-Type': 'multipart/form-data' },
        })
        dispatch({ type: CLEAR_ERRORS })

        dispatch({
            type: ADD_REST_SUCCESS,
            payload: res.data.data,
        })
        dispatch(getAllAdmins())
    } catch (_err) {
        const errors = _err.response.data.error

        dispatch({
            type: GET_ERRORS,
            payload: errors,
        })
    }
}
