import { ADD_FILIAL_SUCCESS, GET_ERRORS, CLEAR_ERRORS, UPDATE_FILIAL_SUCCESS, ADD_FILIAL_CARD } from '../type'
import API from '../../utils/API'
import { loadUser } from './auth'
export const addNewFilial = (restID, data) => async (dispatch) => {
       try {
              const res = await API.post('/filial/' + restID, data, {
                     headers: { 'Content-Type': 'multipart/form-data' },
              })
              dispatch({ type: CLEAR_ERRORS })
              dispatch({
                     type: ADD_FILIAL_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const updateFilial = (filialID, data) => async (dispatch) => {
       try {
              const res = await API.put('/filial/' + filialID, data)
              /* dispatch({ type: CLEAR_ERRORS }) */
              dispatch(loadUser())
              dispatch({
                     type: UPDATE_FILIAL_SUCCESS,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error

              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
export const addCardToFilial = (filialID, data) => async (dispatch) => {
       try {
              const res = await API.put('/filial/card/add/' + filialID, data)
              dispatch(loadUser())
              dispatch({
                     type: ADD_FILIAL_CARD,
                     payload: res.data.data,
              })
       } catch (_err) {
              const errors = _err.response.data.error
              dispatch({
                     type: GET_ERRORS,
                     payload: errors,
              })
       }
}
