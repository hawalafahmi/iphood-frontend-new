import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './providers/store'
import setAuthToken from './utils/setAuthToken'
import { loadUser } from './providers/actions/auth'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'
import './App.scss'
import { Row, Col } from 'reactstrap'
import product from './Components/pages/admin/product/Layout.js'
const loading = () => <div className='animated fadeIn pt-3 text-center'>Loading...</div>

const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'))
const Login = React.lazy(() => import('./Components/auth/login/Login'))

const App = () => {
        localStorage.setItem('token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjYzM2FiODg1NjJjZTBmMmNlMDJlMDdmMSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwiaXNTdXBlckFkbWluIjpmYWxzZSwiaXNBZG1pbiI6dHJ1ZSwicmVzdGF1cmFudCI6WyI2MzNhYjg5ZDYyY2UwZjJjZTAyZTA3ZjQiXX0sImlhdCI6MTY2NTU0ODk1M30.yY590KmLtU0v39ZQvNIQBmR-NZLf2ZzjX3VtVY77C5k')
        useEffect(() => {
                if (localStorage.getItem('token')) {
                        setAuthToken(localStorage.token)

                        store.dispatch(loadUser())
                }
        }, [])
        return (



                <Provider store={store}>
                        <Router>
                                <React.Suspense fallback={loading()}>
                                        <Switch>

                                                <Route exact path='/login' render={(props) => <Login {...props} />} />
                                                <Route path='/' name='Product' render={(props) => <DefaultLayout {...props} />} />
                                        </Switch>
                                </React.Suspense>
                        </Router>

                </Provider>



        )
}

export default App
