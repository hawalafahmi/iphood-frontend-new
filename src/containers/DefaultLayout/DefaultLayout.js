import React, { Suspense, useState, useEffect } from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import * as router from "react-router-dom";
import { Container, Row } from "reactstrap";

import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from "@coreui/react";
// sidebar nav config
import SuperAdmin from "../../navs/supperAdmin.js";
import admin from "../../navs/adminNav.js";
// routes config
import "./layout.css";
import routes from "../../routes";
import { connect } from "react-redux";
import {
  generateAdminNav,
  generateAdminRoutes,
} from "../../navs/generateNav.js";
import AddCategory from "../../Components/pages/admin/Card/add/AddCategory.js";

/* const DefaultFooter = React.lazy(() => import('./DefaultFooter'))
 */

const DefaultHeader = React.lazy(() => import("./DefaultHeader"));
const AddCard = React.lazy(() =>
  import("../../Components/pages/admin/Card/add/AddCard")
);

function DefaultLayout(props) {
  const loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  const [sideBarDetails, setsideBarDetails] = useState({ items: [] });

  useEffect(() => {
    if (props.curentuser) {
      if (props.curentuser.isSuperAdmin === true) {
        setsideBarDetails(SuperAdmin);
      }
      if (props.curentuser.isAdmin === true) {
        generateAdminRoutes(props.curentuser.restaurant, routes);
        setsideBarDetails(generateAdminNav(props.curentuser.restaurant, admin));
      }
    }
  }, [props.curentuser]);
  useEffect(() => {
    if (props.cardAction) {
      /*   let newSideBar = generateAdminNav(props.curentuser.restaurant, admin)
               setsideBarDetails(newSideBar) */
      setTimeout(() => {
        setsideBarDetails({ items: [] });
      });
    }
  }, [props.cardAction]);

  return localStorage.token ? (
    <div className="app">
      <div className="app-body">
        <main className="main">
          <Container fluid>
            <Suspense fallback={loading()}>
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={(props) => (
                        <route.component
                          {...props}
                          componentName={route.name}
                          cardID={route.cardID}
                        />
                      )}
                    />
                  ) : null;
                })}
                {/* <Redirect from="/" to="/product" /> */}
              </Switch>
            </Suspense>
          </Container>
        </main>
      </div>
    </div>
  ) : (
    // <Redirect to="/product" />
    <div className="app">
      <div className="app-body">
        <main className="main">
          <Container fluid>
            <Suspense fallback={loading()}>
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={(props) => (
                        <route.component
                          {...props}
                          componentName={route.name}
                          cardID={route.cardID}
                        />
                      )}
                    />
                  ) : null;
                })}
                {/* <Redirect from="/" to="/product" /> */}
              </Switch>
            </Suspense>
          </Container>
        </main>
      </div>
    </div>
  );
}

DefaultLayout.prototype = {};
const StateProps = (state) => ({
  errors: state.errors,
  auth: state.auth.isAuthenticated,
  curentuser: state.auth.user,
  cardAction: state.card.action,
});
export default connect(StateProps, null)(withRouter(DefaultLayout));
