import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Card, Form, InputGroup, CardTitle, CardBody, CardFooter, Button, Label } from 'reactstrap'
import TextField from '@material-ui/core/TextField'
import _ from 'lodash'
import '../filial.css'
import { Input } from '@material-ui/core'
import FormView from './FormView'
import { withRouter } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'
import PropTpyes from 'prop-types'
import { addNewFilial } from '../../../../../providers/actions/filial'
import { loadUser } from '../../../../../providers/actions/auth'
const FilialForm = ({ user, addNewFilial, errors, history, action, loadUser }) => {
    const dispatch = useDispatch()
    const [Errors, setErrors] = useState({})
    const [FilialObject, setFilialObject] = useState({
        name: '',
        address: '',
        phone: '',
        image: '',
    })
    const onChange = (e) => {
        setFilialObject({ ...FilialObject, [e.target.name]: e.target.value })
    }
    const onSubmit = (e) => {
        var bodyFormData = new FormData()
        bodyFormData.set('name', FilialObject.name)
        bodyFormData.set('address', FilialObject.address)
        bodyFormData.set('phone', FilialObject.phone)
        bodyFormData.append('image', FilialObject.image)

        addNewFilial(user.restaurant[0], bodyFormData)
    }
    useEffect(() => {
        if (errors) setErrors(errors)
        if (action) {
            dispatch({ type: 'ADD_FILIAL_CLEAR' })
            loadUser()
            history.push('/filial/list')
        }
    }, [errors, action, dispatch, history, loadUser])
    return (
        <div className='filial-register-body  animated fadeIn'>
            <Row>
                <Col xs='12' md='4' className='mb-4'>
                    <Card className='form-card'>
                        <div className='form-container'>
                            <Container>
                                <CardTitle>
                                    {' '}
                                    <h5>Add new filial</h5>
                                </CardTitle>
                                <CardBody>
                                    <Form>
                                        <InputGroup className='mb-4'>
                                            <TextField
                                                id='outlined-helperText'
                                                label='Name'
                                                variant='outlined'
                                                name='name'
                                                onChange={(e) => onChange(e)}
                                                error={!_.isNil(Errors.name)}
                                                helperText={Errors.name}
                                            />
                                        </InputGroup>
                                        <InputGroup className='mb-4'>
                                            <TextField
                                                style={{ marginLeft: '1px' }}
                                                id='outlined-helperText'
                                                label='Address'
                                                variant='outlined'
                                                name='address'
                                                onChange={(e) => onChange(e)}
                                                error={!_.isNil(Errors.address)}
                                                helperText={Errors.address}
                                            />
                                        </InputGroup>
                                        <InputGroup className='mb-4'>
                                            <TextField
                                                id='outlined-helperText'
                                                label='Phone'
                                                variant='outlined'
                                                name='phone'
                                                onChange={(e) => onChange(e)}
                                                error={!_.isNil(Errors.phone)}
                                                helperText={Errors.phone}
                                            />
                                        </InputGroup>
                                        <InputGroup className='mb-4'>
                                            <Row>
                                                {' '}
                                                <Label sm='5' size='sm' htmlFor='filePicker' id='upload-image-filial'>
                                                    Choose image...
                                                </Label>
                                            </Row>
                                            <Row>
                                                {' '}
                                                <div style={{ color: 'red', marginTop: '10px', marginLeft: '27px' }}>
                                                    {Errors.image}
                                                </div>
                                            </Row>

                                            <Input
                                                id='filePicker'
                                                style={{ display: 'none' }}
                                                type={'file'}
                                                name='image'
                                                onChange={(e) =>
                                                    setFilialObject({
                                                        ...FilialObject,
                                                        [e.target.name]: e.target.files[0],
                                                    })
                                                }
                                            />
                                        </InputGroup>
                                    </Form>
                                </CardBody>

                                <CardFooter>
                                    <Button className='save-btn' onClick={(e) => onSubmit()}>
                                        save
                                    </Button>
                                    <Button className='cancel-btn' onClick={(e) => history.push('/filial/list')}>
                                        Cancel
                                    </Button>
                                </CardFooter>
                            </Container>
                        </div>
                    </Card>
                </Col>
                <Col xs='12' md='2' className='mb-4'></Col>
                <Col xs='12' md='5' className='mb-4'>
                    {' '}
                    <FormView FilialObject={FilialObject} />{' '}
                </Col>
            </Row>
        </div>
    )
}
FilialForm.prototype = {
    addNewFilial: PropTpyes.func.isRequired,
}
const StateProps = (state) => ({
    errors: state.errors,
    user: state.auth.user,
    action: state.filial.action,
})
export default connect(StateProps, { addNewFilial, loadUser })(withRouter(FilialForm))
