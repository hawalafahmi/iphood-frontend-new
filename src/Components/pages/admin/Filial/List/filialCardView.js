import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import clsx from 'clsx'
import EditFilial from './EditFilial'
import Typography from '@material-ui/core/Typography'
import { red } from '@material-ui/core/colors'
import { Button, Collapse, Icon } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import AsignCard from '../assignCard/AssignCard'
import { connect } from 'react-redux'
import './filialList.css'
const useStyles = makeStyles((theme) => ({
       root: {
              maxWidth: 345,
       },
       media: {
              height: 0,
              paddingTop: '56.25%', // 16:9
       },
       expand: {
              transform: 'rotate(0deg)',
              marginLeft: 'auto',
              transition: theme.transitions.create('transform', {
                     duration: theme.transitions.duration.shortest,
              }),
       },
       expandOpen: {
              transform: 'rotate(180deg)',
       },
       avatar: {
              backgroundColor: red[500],
       },
}))

function FilailCardView({ restaurant, filial, cards }) {
       const [expanded, setExpanded] = React.useState(false)

       const handleExpandClick = () => {
              setExpanded(!expanded)
       }

       const classes = useStyles()

       return (
              <Card className={classes.root}>
                     <CardHeader avatar={<img alt='' src={restaurant.image} id='filial-preview' />} title={filial.name} subheader={filial.phone} />
                     <CardMedia className={classes.media} image={filial.image} title='Paella dish' />
                     <CardContent>
                            <Typography variant='body2' color='textSecondary' component='span'>
                                   {filial.address}
                            </Typography>
                     </CardContent>
                     <CardActions disableSpacing style={{ display: 'block' }}>
                            <Button onClick={handleExpandClick} aria-expanded={expanded} aria-label='show more'>
                                   {' '}
                                   <Icon
                                          className={clsx(classes.expand, {
                                                 [classes.expandOpen]: expanded,
                                          })}
                                   >
                                          <div className='expand-icon'>
                                                 <ExpandMoreIcon />
                                          </div>
                                   </Icon>
                                   Edit
                            </Button>
                            <Button style={{ float: 'right' }}>
                                   {' '}
                                   <AsignCard filial={filial} allCards={cards} />
                            </Button>
                     </CardActions>
                     <Collapse in={expanded} timeout='auto' unmountOnExit>
                            <CardContent>
                                   <EditFilial oldFilial={filial} setExpanded={setExpanded} />
                            </CardContent>
                     </Collapse>
              </Card>
       )
}
FilailCardView.propTypes = {}
const StateProps = (state) => ({
       cards: state.card.currentCards,
})
export default connect(StateProps, null)(withRouter(FilailCardView))
