import React from 'react'
import FilialCardView from './filialCardView'
import { Container } from '@material-ui/core'
import { Row, Col } from 'reactstrap'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

const List = ({ user }) => {
       return (
              <div>
                     <Container>
                            <Row>
                                   {user &&
                                          user.restaurant.map((rest, index) =>
                                                 rest.filials.map((filial, inx) => (
                                                        <Col xs='12' md='4' className='mb-4' key={inx}>
                                                               <FilialCardView filial={filial} restaurant={rest} />
                                                        </Col>
                                                 ))
                                          )}
                            </Row>
                     </Container>
              </div>
       )
}
List.propTypes = {}
const StateProps = (state) => ({
       user: state.auth.user,
})
export default connect(StateProps, null)(withRouter(List))
