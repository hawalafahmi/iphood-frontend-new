import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap'
import { TextField, Container, Button } from '@material-ui/core'
import './filialList.css'
import _ from 'lodash'
import { updateFilial } from '../../../../../providers/actions/filial'
import { connect, useDispatch } from 'react-redux'
import { withRouter } from 'react-router-dom'
const EditFilial = ({ oldFilial, setExpanded, updateFilial, storeFilail, errors }) => {
       const [FilialObject, setFilialObject] = useState(oldFilial)
       const [Errors, setErrors] = useState({})

       const dispatch = useDispatch()
       const onChange = (e) => {
              setFilialObject({ ...FilialObject, [e.target.name]: e.target.value })
       }
       const onSubmit = () => {
              updateFilial(oldFilial._id, FilialObject)
       }
       useEffect(() => {
              if (storeFilail.action === 'UPDATE_FILIAL_SUCCESS') {
                     dispatch({ type: 'ADD_FILIAL_CLEAR' })
                     setExpanded(false)
              }
       }, [storeFilail, setExpanded, dispatch])
       useEffect(() => {
              setErrors(errors)
       }, [errors])
       return (
              <div>
                     <Container>
                            <Row>
                                   <TextField
                                          label='Name'
                                          variant='outlined'
                                          defaultValue={FilialObject.name}
                                          name='name'
                                          onChange={(e) => onChange(e)}
                                          style={{ marginBottom: '10px' }}
                                          error={!_.isNil(Errors.name)}
                                          helperText={Errors.name}
                                   />
                            </Row>
                            <Row>
                                   <TextField
                                          label='Address'
                                          variant='outlined'
                                          defaultValue={FilialObject.address}
                                          name='address'
                                          onChange={(e) => onChange(e)}
                                          style={{ marginBottom: '10px' }}
                                          error={!_.isNil(Errors.address)}
                                          helperText={Errors.address}
                                   />
                            </Row>
                            <Row>
                                   <TextField
                                          label='Phone'
                                          variant='outlined'
                                          defaultValue={FilialObject.phone}
                                          name='phone'
                                          onChange={(e) => onChange(e)}
                                          style={{ marginBottom: '10px' }}
                                          error={!_.isNil(Errors.phone)}
                                          helperText={Errors.phone}
                                   />
                            </Row>
                            <Row>
                                   <Col>
                                          <div className='Filial-Edit-Button-Cancel'>
                                                 <Button onClick={(e) => setExpanded(false)}>Cancel</Button>
                                          </div>
                                   </Col>
                                   <Col>
                                          <div className='Filial-Edit-Button-Save'>
                                                 <Button onClick={(e) => onSubmit()}>Save</Button>
                                          </div>
                                   </Col>
                            </Row>
                     </Container>
              </div>
       )
}

EditFilial.propTypes = {
       updateFilial: PropTypes.func.isRequired,
       errors: PropTypes.object.isRequired,
}
const StateProps = (state) => ({
       storeFilail: state.filial,
       user: state.auth.user,
       errors: state.errors,
})

export default connect(StateProps, { updateFilial })(withRouter(EditFilial))
