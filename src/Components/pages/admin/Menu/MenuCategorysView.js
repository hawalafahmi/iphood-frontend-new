import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { getMenuBuID } from '../../../../providers/actions/menu'
import { Container, Row, Col } from 'reactstrap'
import { ListGroup } from 'reactstrap'
import ProductMenuView from './ProductMenuView'
const MenuCategorysView = ({ currentMenu, menu, getMenuBuID, category }) => {
       useEffect(() => {
              getMenuBuID(currentMenu._id)
       }, [])
       return (
              <div className='current-menu-view-body'>
                     <Container>
                            <ListGroup>
                                   {category && category.menuCategory
                                          ? category.menuCategory.map((cate, index) => (
                                                   <Container className='card-menu-view'>
                                                          {cate.productMenu.length > 0 && (
                                                                 <Row>
                                                                        <h5>{cate.name}</h5> :
                                                                 </Row>
                                                          )}
                                                          <Row>
                                                                 {cate.productMenu &&
                                                                        cate.productMenu.map((product) => (
                                                                               <Col style={{ marginTop: '10px' }}>
                                                                                      <ProductMenuView
                                                                                             product={product}
                                                                                             menu={currentMenu}
                                                                                             categoryMenu={cate}
                                                                                      />
                                                                               </Col>
                                                                        ))}
                                                          </Row>
                                                   </Container>
                                            ))
                                          : null}
                            </ListGroup>
                     </Container>
              </div>
       )
}

MenuCategorysView.propTypes = {
       getMenuBuID: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       menu: state.menu,
       category: state.menu.currentMenu,
})
export default connect(StateProps, { getMenuBuID })(withRouter(MenuCategorysView))
