import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import AddExistCategory from './AddExistCategory'
import AddMenu from './addMenu'
import './menu.css'
import { Container } from '@material-ui/core'
import { Row, Col } from 'reactstrap'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import MenuCategorysView from './MenuCategorysView'

const MenuView = ({ card }) => {
       const classes = useStyles()
       const [value, setValue] = React.useState(0)
       const [CurrentCard, setCurrentCard] = React.useState(null)

       const handleChange = (event, newValue) => {
              setValue(newValue)
       }

       React.useEffect(() => {
              setCurrentCard(card)
       }, [card])

       return (
              <div className='menu-vew-body'>
                     <Row xs='1' sm='2' md='3' className='row-tab-menu'>
                            <Col></Col>
                            <Col sm={{ size: 'auto', offset: 4 }}>{CurrentCard && <AddMenu card={CurrentCard} />}</Col>
                     </Row>
                     <Row>
                            <Tabs
                                   orientation='vertical'
                                   variant='scrollable'
                                   value={value}
                                   onChange={handleChange}
                                   aria-label='Vertical tabs example'
                                   className={classes.tabs}
                            >
                                   {CurrentCard &&
                                          CurrentCard.menu &&
                                          CurrentCard.menu.map((menu, index) => <Tab label={menu.name} {...a11yProps(0)} />)}
                            </Tabs>
                            {CurrentCard &&
                                   CurrentCard.menu &&
                                   CurrentCard.menu.map((menu, index) => (
                                          <TabPanel value={value} index={index} className='vertiacal-menu-tab'>
                                                 <Row xs='1' sm='2' md='3'>
                                                        <Col>
                                                               <MenuCategorysView currentMenu={menu} />
                                                        </Col>
                                                        <Col sm={{ size: 'auto', offset: 4 }}>
                                                               <AddExistCategory category={CurrentCard.category} menuID={menu._id} />
                                                        </Col>
                                                 </Row>
                                          </TabPanel>
                                   ))}
                     </Row>
              </div>
       )
}

MenuView.propTypes = {}

export default MenuView
function TabPanel(props) {
       const { children, value, index, ...other } = props

       return (
              <div role='tabpanel' hidden={value !== index} id={`vertical-tabpanel-${index}`} aria-labelledby={`vertical-tab-${index}`} {...other}>
                     {value === index && (
                            <Box p={3}>
                                   <Typography>{children}</Typography>
                            </Box>
                     )}
              </div>
       )
}

TabPanel.propTypes = {
       children: PropTypes.node,
       index: PropTypes.any.isRequired,
       value: PropTypes.any.isRequired,
}

function a11yProps(index) {
       return {
              id: `vertical-tab-${index}`,
              'aria-controls': `vertical-tabpanel-${index}`,
       }
}

const useStyles = makeStyles((theme) => ({
       root: {
              flexGrow: 1,
              backgroundColor: theme.palette.background.paper,
              display: 'flex',
              height: 224,
       },
       tabs: {
              minHeight: '34.5em',
              borderRight: `1px solid ${theme.palette.divider}`,
       },
}))
