import React, { useState } from 'react'
import { Container } from 'reactstrap'
const ImgView = ({ ImgObject }) => {
     const [img, setimg] = useState(null)
     if (ImgObject.image) {
          const reader = new FileReader()

          reader.readAsDataURL(ImgObject.image)
          reader.onloadend = () => {
               setimg(reader.result)
          }
     }
     return (
          <div>
               <Container>
                    {img ? (
                         <img alt='' src={img} style={{ width: '21em' }} />
                    ) : (
                         <img alt='' style={{ width: '21em' }} src='https://via.placeholder.com/150' />
                    )}
               </Container>
          </div>
     )
}

export default ImgView
