/* eslint-disable react-hooks/exhaustive-deps */
import AddIcon from '@material-ui/icons/Add'
import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { InputGroup, Form, Row, Label, Col } from 'reactstrap'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

import { TextField, Input } from '@material-ui/core'
import _ from 'lodash'
import { withRouter } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'
import '../card.css'
import { addNewCategory } from '../../../../../providers/actions/category'
const useStyles = makeStyles((theme) => ({
       root: {
              '& > *': {
                     margin: theme.spacing(1),
              },
       },
       extendedIcon: {
              marginRight: theme.spacing(1),
       },
}))

const AddCategory = ({ addNewCategory, errors, cardID, refresh }) => {
       const classes = useStyles()
       const [Errors, setErrors] = useState({})
       const [CategoryObject, setCategoryObject] = useState({
              name: '',
              description: '',
              image: '',
       })
       const [modal, setModal] = useState(false)

       const toggle = () => {
              setModal(!modal)
              setCategoryObject({
                     name: '',
                     description: '',
                     image: '',
              })
       }
       const dispatch = useDispatch()

       const onChange = (e) => {
              setCategoryObject({ ...CategoryObject, [e.target.name]: e.target.value })
       }
       const onSubmit = (e) => {
              var bodyFormData = new FormData()
              bodyFormData.set('name', CategoryObject.name)
              bodyFormData.set('description', CategoryObject.description)
              bodyFormData.append('image', CategoryObject.image)
              addNewCategory(cardID, bodyFormData)
       }
       useEffect(() => {
              if (errors) setErrors(errors)
       }, [errors])
       useEffect(() => {
              if (refresh) {
                     dispatch({ type: 'ADD_CATEGORY_REFRESH' })
                     toggle()
              }
       }, [refresh])
       return (
              <div className={classes.root}>
                     <Button onClick={toggle} className='add-category'>
                            <Row>
                                   <Col>
                                          <div className='add-category-text'>create new Category</div>
                                   </Col>
                                   <Col xs='2'>
                                          <div className='add-category-icon'>
                                                 <AddIcon />
                                          </div>
                                   </Col>
                            </Row>
                     </Button>
                     <Form>
                            <Modal isOpen={modal} toggle={toggle}>
                                   <ModalHeader toggle={toggle}>Add new Category </ModalHeader>
                                   <ModalBody>
                                          <InputGroup className='mb-4'>
                                                 <TextField
                                                        id='outlined-helperText'
                                                        label='Name'
                                                        variant='outlined'
                                                        name='name'
                                                        onChange={(e) => onChange(e)}
                                                        error={!_.isNil(Errors.name)}
                                                        helperText={Errors.name}
                                                 />
                                          </InputGroup>
                                          <InputGroup className='mb-4'>
                                                 <TextField
                                                        style={{ marginLeft: '1px' }}
                                                        id='outlined-helperText'
                                                        label='description'
                                                        variant='outlined'
                                                        name='description'
                                                        onChange={(e) => onChange(e)}
                                                        error={!_.isNil(Errors.description)}
                                                        helperText={Errors.description}
                                                 />
                                          </InputGroup>

                                          <InputGroup className='mb-4'>
                                                 <Row>
                                                        {' '}
                                                        <Label sm='5' size='sm' htmlFor='filePicker' id='upload-image-filial'>
                                                               Choose image...
                                                        </Label>
                                                 </Row>
                                                 <Row>
                                                        {' '}
                                                        <div style={{ color: 'red', marginTop: '10px', marginLeft: '27px' }}>{Errors.image}</div>
                                                 </Row>

                                                 <Input
                                                        id='filePicker'
                                                        style={{ display: 'none' }}
                                                        type={'file'}
                                                        name='image'
                                                        onChange={(e) =>
                                                               setCategoryObject({
                                                                      ...CategoryObject,
                                                                      [e.target.name]: e.target.files[0],
                                                               })
                                                        }
                                                 />
                                          </InputGroup>
                                   </ModalBody>
                                   <ModalFooter>
                                          <Button className='cancel-btn' onClick={toggle}>
                                                 Cancel
                                          </Button>
                                          <Button className='save-btn' onClick={(e) => onSubmit()}>
                                                 save
                                          </Button>
                                   </ModalFooter>
                            </Modal>
                     </Form>
              </div>
       )
}

AddCategory.prototype = {
       addNewCategory: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       errors: state.errors,
       user: state.auth.user,
       refresh: state.category.refresh,
})
export default connect(StateProps, { addNewCategory })(withRouter(AddCategory))
