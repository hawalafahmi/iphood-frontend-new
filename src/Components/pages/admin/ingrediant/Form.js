/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect, useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { useTheme, makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import { addNewIngredient } from '../../../../providers/actions/ingredient'

export const Form = ({ addNewIngredient, action, newIN }) => {
       const [Ingredient, setIngredient] = useState({ name: '', price: '' })
       const [open, setOpen] = React.useState(false)
       const onChange = (e) => setIngredient({ ...Ingredient, [e.target.name]: e.target.value })
       const theme = useTheme()
       const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
       const classes = useStyles()
       const dispatch = useDispatch()
       const onSubmit = (e) => addNewIngredient(Ingredient)
       const handleClickOpen = () => setOpen(true)
       const handleClose = () => {
              setOpen(false)
              setIngredient({ name: '', price: '' })
       }
       useEffect(() => {
              if (action === 'ADD_INGREDIENT_SUCCESS') {
                     dispatch({ type: 'ADD_INGREDIENT_CLEAR' })
                     handleClose()
              }
       }, [action])

       return (
              <div>
                     <Button variant='outlined' color='primary' onClick={handleClickOpen}>
                            Create new ingredient
                     </Button>
                     <Dialog fullScreen={fullScreen} open={open} onClose={handleClose} aria-labelledby='responsive-dialog-title'>
                            <DialogTitle id='responsive-dialog-title'>{'Create new ingredient'}</DialogTitle>
                            <DialogContent>
                                   <DialogContentText>
                                          <form className={classes.root} noValidate autoComplete='off'>
                                                 <TextField
                                                        id='outlined-basic'
                                                        name='name'
                                                        label='name'
                                                        value={Ingredient.name}
                                                        variant='outlined'
                                                        onChange={(e) => onChange(e)}
                                                 />
                                                 <TextField
                                                        id='outlined-basic'
                                                        name='price'
                                                        label='price'
                                                        value={Ingredient.price}
                                                        variant='outlined'
                                                        onChange={(e) => onChange(e)}
                                                 />
                                          </form>
                                   </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                   <Button autoFocus onClick={handleClose} color='primary'>
                                          cancel
                                   </Button>
                                   <Button onClick={(e) => onSubmit()} color='primary' autoFocus>
                                          Save
                                   </Button>
                            </DialogActions>
                     </Dialog>
              </div>
       )
}

Form.propTypes = {
       addNewIngredient: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
       newIN: state.ingredient.new,
       action: state.ingredient.action,
})

export default connect(mapStateToProps, {
       addNewIngredient,
})(Form)
const useStyles = makeStyles((theme) => ({
       root: {
              '& > *': {
                     margin: theme.spacing(1),
                     width: '25em',
              },
       },
}))
