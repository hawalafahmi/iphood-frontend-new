/* eslint-disable react-hooks/exhaustive-deps */
import React, { Component, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import Form from './Form'
import View from './View'
import { Container } from '@material-ui/core'
import { Row, Col } from 'reactstrap'
import Proptypes from 'prop-types'
import './ingredient.css'
import { getAllIngredients } from '../../../../providers/actions/ingredient'
export const Layout = ({ allIngredients, getAllIngredients }) => {
         const [IngredientObject, setIngredientObject] = useState()
         useEffect(() => {
                  getAllIngredients()
         }, [])
         useEffect(() => {
                  if (allIngredients) setIngredientObject(allIngredients)
         }, [allIngredients])
         return (
                  <div>
                           <Container>
                                    <Row xs='1' sm='2' md='3' className='row-tab-menu'>
                                             <Col></Col>
                                             <Col sm={{ size: 'auto', offset: 4 }}>
                                                      {' '}
                                                      <Form />
                                             </Col>
                                    </Row>

                                    {IngredientObject && <View IngredientObject={IngredientObject.reverse()} />}
                           </Container>
                  </div>
         )
}
Layout.prototype = {
         getAllIngredients: Proptypes.func.isRequired,
}
const mapStateToProps = (state) => ({
         allIngredients: state.ingredient.all,
})

export default connect(mapStateToProps, {
         getAllIngredients,
})(Layout)
