import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Actions from './Actions'
import { Chip } from '@material-ui/core'

export const View = ({ IngredientObject }) => {
        const classes = useStyles()

        var rows = []
        IngredientObject.map((oneIngredient) =>
                rows.push(
                        createData(
                                oneIngredient.name,
                                oneIngredient.pickedBy,
                                oneIngredient.status,
                                oneIngredient.createdAt,
                                oneIngredient.price,
                                <Actions ingID={oneIngredient._id} currentIngredient={oneIngredient} />
                        )
                )
        )
        return (
                <div className='ingredient-table-view'>
                        <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label='simple table'>
                                        <TableHead>
                                                <TableRow>
                                                        <TableCell>Ingredient Name</TableCell>
                                                        <TableCell>Handled By</TableCell>
                                                        <TableCell>Status</TableCell>
                                                        <TableCell align='right'>Created At</TableCell>
                                                        <TableCell align='right'>Price</TableCell>
                                                        <TableCell align='right'>actions</TableCell>
                                                </TableRow>
                                        </TableHead>
                                        <TableBody>
                                                {rows.map((row) => (
                                                        <TableRow key={row.name}>
                                                                <TableCell component='th' scope='row'>
                                                                        {row.name}
                                                                </TableCell>
                                                                <TableCell component='th' scope='row'>
                                                                        {row.pickedBy &&
                                                                                row.pickedBy.map((product) => <div>{product.name} </div>)}
                                                                </TableCell>
                                                                <TableCell component='th' scope='row'>
                                                                        {row.status==='active' ? <Chip   className='active_tag'   label="Active"/> : <Chip    className='unactive_tag'   label="unactive"/>}
                                                                </TableCell>
                                                                <TableCell align='right'>{row.createdAt}</TableCell>
                                                                <TableCell align='right'>{row.price}</TableCell>
                                                                <TableCell align='right'>{row.actions}</TableCell>
                                                        </TableRow>
                                                ))}
                                        </TableBody>
                                </Table>
                        </TableContainer>
                </div>
        )
}

export default connect(null, null)(View)
const useStyles = makeStyles({
        table: {
                minWidth: 650,
        },
})

const createData = (name, pickedBy, status, createdAt, price, actions) => {
        return { name, pickedBy, status, createdAt, price, actions }
}
