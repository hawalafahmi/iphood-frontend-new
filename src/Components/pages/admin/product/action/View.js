import React from 'react'
import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Menu from '@material-ui/core/Menu'
import DeleteProduct from './DeleteProduct'
import EditPorduct from './EditPorduct'
const View = ({ category, product, fromCategory, cardID }) => {
       const [anchorEl, setAnchorEl] = React.useState(null)

       const handleClick = (event) => {
              setAnchorEl(event.currentTarget)
       }

       const handleClose = () => {
              setAnchorEl(null)
       }
       return (
              <div>
                     <IconButton component={'span'} aria-label='settings' onClick={handleClick}>
                            <MoreVertIcon />
                     </IconButton>
                     <Menu id='simple-menu' anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                            <DeleteProduct
                                   handleCloseMenu={handleClose}
                                   category={category}
                                   product={product}
                                   fromCategory={fromCategory}
                                   cardID={cardID}
                            />
                            <EditPorduct
                                   handleCloseMenu={handleClose}
                                   category={category}
                                   product={product}
                                   fromCategory={fromCategory}
                                   cardID={cardID}
                            />
                     </Menu>
              </div>
       )
}

View.propTypes = {}

export default View
