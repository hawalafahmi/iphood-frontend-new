import React, { Component, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import MenuItem from '@material-ui/core/MenuItem'
import { removeProductFromCategory, deleteProduct } from '../../../../../providers/actions/product'
const DeleteProduct = ({ fromCategory, product, category, handleCloseMenu, removeProductFromCategory, cardID, deleteProduct }) => {
       const [open, setOpen] = React.useState(false)
       const [ProductObj, setProductObj] = React.useState(product)

       const handleClickOpen = () => {
              setOpen(true)
       }

       const handleClose = () => {
              setOpen(false)
       }
       useEffect(() => {
              if (product) setProductObj(product)
       }, [product])
       const removeProduct = () => (fromCategory ? removeProductFromCategory(cardID, category._id, ProductObj._id) : deleteProduct(ProductObj._id))
       return (
              <div>
                     {' '}
                     <MenuItem
                            onClick={(e) => {
                                   handleClickOpen()
                                   handleCloseMenu()
                            }}
                     >
                            {fromCategory ? <div>Remove</div> : <div>delete</div>}
                     </MenuItem>
                     <Dialog
                            open={open}
                            TransitionComponent={Transition}
                            keepMounted
                            onClose={handleClose}
                            aria-labelledby='alert-dialog-slide-title'
                            aria-describedby='alert-dialog-slide-description'
                     >
                            <DialogTitle id='alert-dialog-slide-title'>{'Are You Sure ?'}</DialogTitle>
                            <DialogContent>
                                   <DialogContentText id='alert-dialog-slide-description'>
                                          {fromCategory ? (
                                                 <div>
                                                        {' '}
                                                        This Product Is Gonna Be Removed Only From This Category If You Want To Remove It Completely
                                                        It You Have To Delete From Product Main Page
                                                 </div>
                                          ) : (
                                                 <div> if you delete this product it will be deleted from any dependency</div>
                                          )}
                                   </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                   <Button
                                          onClick={(e) => {
                                                 handleClose()
                                          }}
                                          color='primary'
                                   >
                                          Disagree
                                   </Button>
                                   <Button
                                          onClick={(e) => {
                                                 handleClose()
                                                 removeProduct()
                                          }}
                                          color='primary'
                                   >
                                          Agree
                                   </Button>
                            </DialogActions>
                     </Dialog>
              </div>
       )
}

DeleteProduct.prototype = {
       removeProductFromCategory: PropTypes.func.isRequired,
       deleteProduct: PropTypes.func.isRequired,
}
const mapStateToProps = (state) => ({})
const mapDispatchToProps = {
       removeProductFromCategory,
       deleteProduct,
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteProduct)

const Transition = React.forwardRef(function Transition(props, ref) {
       return <Slide direction='up' ref={ref} {...props} />
})
