/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./product.css";
import Form from "./Form";
import View from "./View";
import { getAllProduct } from "../../../../providers/actions/product";
import { Row, Col } from "reactstrap";
import TextField from "@material-ui/core/TextField";
import Card from "../Card/Card";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
//tabpanel start
//FILTER STUFF START

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";

import burger from "../../../../assets/img/brand/burger.png";
import resLogo from "../../../../assets/img/brand/iphood.png";
import iphoodbanner from "../../../../assets/img/brand/iphoodbanner.jpg";
import shoppingcart from "../../../../assets/img/brand/shopping-cart.png";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
//CART STUFF START
// const [price, setPrice] = useState(0)
// console.log(price)
// const getdata = localStorage.getItem('productsInCart');
// console.log(getdata);

// const [anchorEl, setAnchorEl] = useState(null);
// const open = Boolean(anchorEl);
// const handleClick = (event) => {
//   setAnchorEl(event.currentTarget);
// };
// const handleClose = () => {
//   setAnchorEl(null);
// };

// const dlt = (id) => {
//   dispatch(DLT(id))

// }

// const total = () => {
//   let price = 0;
//   getdata.map((ele, k) => {
//     price = ele.price + price
//   })
//   setPrice(price)
// }

// useEffect(() => {
//   total()

// }, [total])

//CART STUFF END
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  "& > *": {
    margin: theme.spacing(1),
    width: "25ch",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

//FILTER STUFF END
//tabpanel end
const Layout = ({ allProducts, getAllProduct }) => {
  const [Products, setProducts] = useState();
  useEffect(() => {
    if (allProducts) setProducts(allProducts);
  }, [allProducts]);
  useEffect(() => {
    getAllProduct();
  }, []);
  //FILTER STUFF
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  //FILTER STUFF

  //RADIO BUTTON START
  const [FormValue, setFormValue] = React.useState("clickandcollect");

  const handleFormChange = (event) => {
    setFormValue(event.target.value);
  };
  //RADIO BUTTON END

  //CART STUFF START
  function getCartItems() {
    const cartData = localStorage.getItem("productsInCart");
    if (cartData) {
      setCart(JSON.parse(cartData));
    }
  }

  const initialState = [];
  const [cart, setCart] = useState(initialState);

  useEffect(() => {
    getCartItems();
  }, []);
  window.addEventListener("storage", () => {
    getCartItems();
  });
  useEffect(() => {
    if (cart !== initialState) {
      localStorage.setItem("productsInCart", JSON.stringify(cart));
    }
  }, [cart]);
  //CART STUFF END

  //TOTAL SUM START
  const [price, setPrice] = useState(0);
  console.log(price);
  const total = () => {
    let price = 0;
    cart.map((ele, k) => {
      price = eval(ele.price * ele.qty) + eval(price);
    });
    setPrice(price);
  };

  useEffect(() => {
    total();
  }, [total]);
  //TOTAL SUM END
  //checkout button handler
  function checkoutButtonHandler() {
    localStorage.setItem("productsInCart", "[]");
    getCartItems();
  }
  function removeProductFromCart(id) {
    const existingData =
      JSON.parse(localStorage.getItem("productsInCart")) || [];
    const filteredData = existingData.filter((data) => data.id !== id);
    localStorage.setItem("productsInCart", JSON.stringify(filteredData));
    getCartItems();
  }
  //checkout button handler

  //total number in cart with quantity
  // console.log(Object.keys("productsInCart").length + " items in cart");

  //total number in cart without quantity

  // console.log(
  //   Object?.keys(JSON.parse(localStorage?.getItem("productsInCart")))?.length,
  //   "items in cart"
  // );

  //modal stuff start
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //modal stuff end
  return (
    <div>
      <div id="header">
        <div id="restaurantLogoContainer">
          <img src={resLogo} id="restaurantLogo" />
        </div>
        {/* <div id="searchBarContainer">
          <input
            type="search"
            id="searchBar"
            class="form-control"
            placeholder="Chercher"
          />
        </div>
        <div id="profileContainer">
          <IconButton
            aria-label="account of current user"
            aria-controls="primary-search-account-menu"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </div> */}
        <div
          id="desktopCartContainer"
          class="DesktopTabletOnly"
          onClick={() => {
            handleOpen();
          }}
        >
          <IconButton
            color="secondary"
            aria-label="add to shopping cart"
            id="cartDesktopBtn"
          >
            <ShoppingBasketIcon /> &nbsp; &nbsp; {price} €
          </IconButton>
        </div>
      </div>

      <div class="row">
        {/* <div class="col-lg-8 col-md-12"> */}
        <div id="ProductsFilterContainer" className={classes.root}>
          {/* <AppBar position="static">
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="simple tabs example"
              >
                <Tab label="Burgers" className="tabBurgers" {...a11yProps(0)} />
                <Tab label="Pizzas" className="tabPizzas" {...a11yProps(1)} />
                <Tab label="Sodas" className="tabSodas" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              {Products && <View Products={Products} />}
            </TabPanel>
            <TabPanel value={value} index={1}>
              Item Two
            </TabPanel>
            <TabPanel value={value} index={2}>
              Item Three
            </TabPanel> */}

          {/* <div id="bannerContainer">
              <img src={iphoodbanner} />
            </div> */}
          <Card />
        </div>
        <div
          class="MobileOnly"
          id="shoopingCartIconContainer"
          onClick={handleOpen}
        >
          <span id="cartQuantityMobile">
            {(() => {
              if (JSON?.parse(localStorage?.getItem("productsInCart"))) {
                return Object?.keys(
                  JSON?.parse(localStorage?.getItem("productsInCart"))
                )?.length;
              } else {
                return 0;
              }
            })()}
          </span>
          {/* <img src={shoppingcart} /> */}
          <button id="cartBtnMobile">Voir Panier</button>
        </div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div className={classes.paper}>
              <div class="card my-cart-card">
                <div class="card-header d-flex justify-content-between align-items-center pb-0  border-bottom-0">
                  <span class="MuiTypography-root MuiCardHeader-title MuiTypography-h5 MuiTypography-displayBlock">
                    Panier
                  </span>
                  <span
                    className="MobileOnly"
                    id="closeModalButton"
                    onClick={handleClose}
                  >
                    X
                  </span>
                </div>
                <div class="card-body ">
                  {(() => {
                    if (!(JSON?.parse(localStorage?.getItem("productsInCart"))) || (JSON?.parse(localStorage?.getItem("productsInCart"))).length === 0) {
                      return <div id="emptyCartText">Vous n'avez encore rien dans votre panier!</div>
                    }
                  })()}
                  <div>
                    {
                      cart.map((e) => {
                        return (
                          <>
                            <div class="rounded-pill bg-soft-primary iq-my-cart">
                              <div class="d-flex align-items-center justify-content-between profile-img4 relative">
                                <div class="profile-img11 img-container">
                                  <img
                                    src={e.picture}
                                    class="img-fluid rounded-pill avatar-115"
                                    alt="img"
                                  />
                                </div>
                                <div class="d-flex align-items-center profile-content">
                                  <div>
                                    <h6 class="mb-1 heading-title fw-bolder">
                                      {e.name}
                                    </h6>
                                    <span class="d-flex align-items-center ">
                                      <span class="xTimes">X</span>
                                      <span class="text-dark ms-1">{e.qty}</span>
                                    </span>
                                  </div>
                                </div>
                                <div class="me-4 text-end">
                                  <span
                                    class="mb-1"
                                    style={{ cursor: "pointer" }}
                                    onClick={() => {
                                      removeProductFromCart(e.id);
                                    }}
                                  >
                                    <svg
                                      width="20"
                                      height="20"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path
                                        opacity="0.4"
                                        d="M19.6449 9.48924C19.6449 9.55724 19.112 16.298 18.8076 19.1349C18.6169 20.8758 17.4946 21.9318 15.8111 21.9618C14.5176 21.9908 13.2514 22.0008 12.0055 22.0008C10.6829 22.0008 9.38936 21.9908 8.1338 21.9618C6.50672 21.9228 5.38342 20.8458 5.20253 19.1349C4.88936 16.288 4.36613 9.55724 4.35641 9.48924C4.34668 9.28425 4.41281 9.08925 4.54703 8.93126C4.67929 8.78526 4.86991 8.69727 5.07026 8.69727H18.9408C19.1402 8.69727 19.3211 8.78526 19.464 8.93126C19.5973 9.08925 19.6644 9.28425 19.6449 9.48924"
                                        fill="#E60A0A"
                                      ></path>
                                      <path
                                        d="M21 5.97686C21 5.56588 20.6761 5.24389 20.2871 5.24389H17.3714C16.7781 5.24389 16.2627 4.8219 16.1304 4.22692L15.967 3.49795C15.7385 2.61698 14.9498 2 14.0647 2H9.93624C9.0415 2 8.26054 2.61698 8.02323 3.54595L7.87054 4.22792C7.7373 4.8219 7.22185 5.24389 6.62957 5.24389H3.71385C3.32386 5.24389 3 5.56588 3 5.97686V6.35685C3 6.75783 3.32386 7.08982 3.71385 7.08982H20.2871C20.6761 7.08982 21 6.75783 21 6.35685V5.97686Z"
                                        fill="#E60A0A"
                                      ></path>
                                    </svg>
                                  </span>
                                  <p class="mb-0 text-dark">
                                    {" "}
                                    {e.price * e.qty} €
                                  </p>
                                </div>
                              </div>
                            </div>
                          </>
                        );
                      })}
                  </div>
                  {(() => {
                    if ((JSON?.parse(localStorage?.getItem("productsInCart"))) && (JSON?.parse(localStorage?.getItem("productsInCart"))).length != 0) {
                      return (
                        <div class="my-cart-body">
                          <div class="border border-primary rounded p-3 mt-5">
                            <div>
                              <FormControl component="fieldset">
                                <FormLabel component="legend">
                                  Mode de consammation
                                </FormLabel>
                                <RadioGroup
                                  aria-label="gender"
                                  name="gender1"
                                  value={FormValue}
                                  onChange={handleFormChange}
                                >
                                  <FormControlLabel
                                    value="clickandcollect"
                                    control={<Radio />}
                                    label="Click & Collect"
                                  />
                                  <FormControlLabel
                                    value="consommationsurplace"
                                    control={<Radio />}
                                    label="Consommation sur place"
                                  />

                                  <FormControlLabel
                                    value="disabled"
                                    disabled
                                    control={<Radio />}
                                    label="Livraison à domicile"
                                  />
                                </RadioGroup>
                              </FormControl>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mb-3">
                              <div class="couponCodeContainer">
                                {" "}
                                <label
                                  class="htmlForm-check-label"
                                  for="exampleRadio1"
                                >
                                  Code Promo
                                </label>
                                <form
                                  className={classes.root}
                                  noValidate
                                  autoComplete="off"
                                >
                                  <TextField id="standard-basic" label="Coupon" />
                                </form>
                              </div>
                              <h6 class="heading-title fw-bolder text-primary">
                                0 €
                              </h6>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mb-2">
                              <h6 class="heading-title fw-bolder darkBlue">Total</h6>
                              <h6 class="heading-title fw-bolder text-primary">
                                {price} €
                              </h6>
                            </div>
                            <div class="d-flex justify-content-between align-items-center ">
                              <h6 class="heading-title fw-bolder darkBlue">Final</h6>
                              <h6 class="heading-title fw-bolder text-primary">
                                {price} €
                              </h6>
                            </div>
                          </div>
                          <div class="text-center mt-3">
                            <button
                              type="button"
                              class="btn btn-primary rounded-pill"
                              onClick={() => {
                                checkoutButtonHandler();
                              }}
                            >
                              Payer
                            </button>
                          </div>
                        </div>
                      );

                    }
                  })()}

                </div>
              </div>
            </div>
          </Fade>
        </Modal>
        {/* </div> */}

        {/* <div class="col-lg-4 col-md-12" id="cartContainer">
          <div class="card my-cart-card">
            <div class="card-header d-flex justify-content-between align-items-center pb-0  border-bottom-0">
              <span class="MuiTypography-root MuiCardHeader-title MuiTypography-h5 MuiTypography-displayBlock">
                Panier
              </span>
            </div>
            <div class="card-body ">
              <div>
                {cart.map((e) => {
                  return (
                    <>
                      <div class="rounded-pill bg-soft-primary iq-my-cart">
                        <div class="d-flex align-items-center justify-content-between profile-img4 relative">
                          <div class="profile-img11 img-container">
                            <img
                              src={e.picture}
                              class="img-fluid rounded-pill avatar-115"
                              alt="img"
                            />
                          </div>
                          <div class="d-flex align-items-center profile-content">
                            <div>
                              <h6 class="mb-1 heading-title fw-bolder">
                                {e.name}
                              </h6>
                              <span class="d-flex align-items-center ">
                                <span class="xTimes">X</span>
                                <span class="text-dark ms-1">{e.qty}</span>
                              </span>
                            </div>
                          </div>
                          <div class="me-4 text-end">
                            <span
                              class="mb-1"
                              style={{ cursor: "pointer" }}
                              onClick={() => {
                                removeProductFromCart(e.id);
                              }}
                            >
                              <svg
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  opacity="0.4"
                                  d="M19.6449 9.48924C19.6449 9.55724 19.112 16.298 18.8076 19.1349C18.6169 20.8758 17.4946 21.9318 15.8111 21.9618C14.5176 21.9908 13.2514 22.0008 12.0055 22.0008C10.6829 22.0008 9.38936 21.9908 8.1338 21.9618C6.50672 21.9228 5.38342 20.8458 5.20253 19.1349C4.88936 16.288 4.36613 9.55724 4.35641 9.48924C4.34668 9.28425 4.41281 9.08925 4.54703 8.93126C4.67929 8.78526 4.86991 8.69727 5.07026 8.69727H18.9408C19.1402 8.69727 19.3211 8.78526 19.464 8.93126C19.5973 9.08925 19.6644 9.28425 19.6449 9.48924"
                                  fill="#E60A0A"
                                ></path>
                                <path
                                  d="M21 5.97686C21 5.56588 20.6761 5.24389 20.2871 5.24389H17.3714C16.7781 5.24389 16.2627 4.8219 16.1304 4.22692L15.967 3.49795C15.7385 2.61698 14.9498 2 14.0647 2H9.93624C9.0415 2 8.26054 2.61698 8.02323 3.54595L7.87054 4.22792C7.7373 4.8219 7.22185 5.24389 6.62957 5.24389H3.71385C3.32386 5.24389 3 5.56588 3 5.97686V6.35685C3 6.75783 3.32386 7.08982 3.71385 7.08982H20.2871C20.6761 7.08982 21 6.75783 21 6.35685V5.97686Z"
                                  fill="#E60A0A"
                                ></path>
                              </svg>
                            </span>
                            <p class="mb-0 text-dark"> {e.price * e.qty} €</p>
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })}
              </div>

              <div class="my-cart-body">
                <div class="border border-primary rounded p-3 mt-5">
                  <div>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">
                        Mode de consammation
                      </FormLabel>
                      <RadioGroup
                        aria-label="gender"
                        name="gender1"
                        value={FormValue}
                        onChange={handleFormChange}
                      >
                        <FormControlLabel
                          value="clickandcollect"
                          control={<Radio />}
                          label="Click & Collect"
                        />
                        <FormControlLabel
                          value="consommationsurplace"
                          control={<Radio />}
                          label="Consommation sur place"
                        />

                        <FormControlLabel
                          value="disabled"
                          disabled
                          control={<Radio />}
                          label="Livraison à domicile"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                  <div class="d-flex justify-content-between align-items-center mb-3">
                    <div class="couponCodeContainer">
                      {" "}
                      <label class="htmlForm-check-label" for="exampleRadio1">
                        Code Promo
                      </label>
                      <form
                        className={classes.root}
                        noValidate
                        autoComplete="off"
                      >
                        <TextField id="standard-basic" label="Coupon" />
                      </form>
                    </div>
                    <h6 class="heading-title fw-bolder text-primary">0 €</h6>
                  </div>
                  <div class="d-flex justify-content-between align-items-center mb-2">
                    <h6 class="heading-title fw-bolder darkBlue">Total</h6>
                    <h6 class="heading-title fw-bolder text-primary">
                      {price} €
                    </h6>
                  </div>
                  <div class="d-flex justify-content-between align-items-center ">
                    <h6 class="heading-title fw-bolder darkBlue">Final</h6>
                    <h6 class="heading-title fw-bolder text-primary">
                      {price} €
                    </h6>
                  </div>
                </div>
                <div class="text-center mt-3">
                  <button
                    type="button"
                    class="btn btn-primary rounded-pill"
                    onClick={() => {
                      checkoutButtonHandler();
                    }}
                  >
                    Payer
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
};
Layout.prototype = {
  getAllProduct: PropTypes.func.isRequired,
};
const StateProps = (state) => ({
  errors: state.errors,
  allProducts: state.product.allProducts,
});
export default connect(StateProps, {
  getAllProduct,
})(withRouter(Layout));
