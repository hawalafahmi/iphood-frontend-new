import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import _ from 'lodash'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import { removeIngredientFromProduct } from '../../../../providers/actions/ingredient'
import { connect } from 'react-redux'

const ViewIngredient = ({ productIngredient, removeIngredientFromProduct }) => {
       const classes = useStyles()
       const [modal, setModal] = useState(false)
       const toggle = () => setModal(!modal)

       return (
              <div className={classes.root}>
                     <Button
                            color='danger'
                            className='view-product-ingredient'
                            onClick={toggle}
                            disabled={
                                   productIngredient['ingredientObligatoire'].length === 0 &&
                                   productIngredient['ingredientOptional'].length === 0 &&
                                   productIngredient['ingredientExtrat'].length === 0
                            }
                     >
                            View Ingredient{' '}
                     </Button>
                     {productIngredient && (
                            <Modal isOpen={modal} toggle={toggle}>
                                   <ModalHeader toggle={toggle}>Ingredient list </ModalHeader>
                                   <ModalBody>
                                          Obligatoire
                                          {productIngredient['ingredientObligatoire'].map((ing, inx) => (
                                                 <ExpansionPanel>
                                                        <ExpansionPanelSummary
                                                               expandIcon={<ExpandMoreIcon />}
                                                               aria-controls='panel1a-content'
                                                               id='panel1a-header'
                                                        >
                                                               <Typography className={classes.heading}>{ing.name}</Typography>
                                                        </ExpansionPanelSummary>
                                                        <ExpansionPanelDetails>
                                                               <Typography>
                                                                      {' '}
                                                                      <Button
                                                                             onClick={(e) =>
                                                                                    removeIngredientFromProduct(ing._id, productIngredient._id, {
                                                                                           ingredientObligatoire: [ing._id],
                                                                                    })
                                                                             }
                                                                      >
                                                                             {' '}
                                                                             Remove
                                                                      </Button>
                                                               </Typography>
                                                        </ExpansionPanelDetails>
                                                 </ExpansionPanel>
                                          ))}
                                          Optionnel
                                          {productIngredient['ingredientOptional'].map((ing, inx) => (
                                                 <ExpansionPanel>
                                                        <ExpansionPanelSummary
                                                               expandIcon={<ExpandMoreIcon />}
                                                               aria-controls='panel1a-content'
                                                               id='panel1a-header'
                                                        >
                                                               <Typography className={classes.heading}>{ing.name}</Typography>
                                                        </ExpansionPanelSummary>
                                                        <ExpansionPanelDetails>
                                                               <Typography>
                                                                      <Button
                                                                             onClick={(e) =>
                                                                                    removeIngredientFromProduct(ing._id, productIngredient._id, {
                                                                                           ingredientOptional: [ing._id],
                                                                                    })
                                                                             }
                                                                      >
                                                                             {' '}
                                                                             Remove
                                                                      </Button>
                                                               </Typography>
                                                        </ExpansionPanelDetails>
                                                 </ExpansionPanel>
                                          ))}
                                          Extra
                                          {productIngredient['ingredientExtrat'].length > 0 &&
                                                 productIngredient['ingredientExtrat'].map((ing, inx) => (
                                                        <ExpansionPanel>
                                                               <ExpansionPanelSummary
                                                                      expandIcon={<ExpandMoreIcon />}
                                                                      aria-controls='panel1a-content'
                                                                      id='panel1a-header'
                                                               >
                                                                      <Typography className={classes.heading}>{ing.name}</Typography>
                                                               </ExpansionPanelSummary>
                                                               <ExpansionPanelDetails>
                                                                      <Typography>
                                                                             <Button
                                                                                    onClick={(e) =>
                                                                                           removeIngredientFromProduct(
                                                                                                  ing._id,
                                                                                                  productIngredient._id,
                                                                                                  { ingredientExtrat: [ing._id] }
                                                                                           )
                                                                                    }
                                                                             >
                                                                                    {' '}
                                                                                    Remove
                                                                             </Button>
                                                                      </Typography>
                                                               </ExpansionPanelDetails>
                                                        </ExpansionPanel>
                                                 ))}
                                   </ModalBody>
                            </Modal>
                     )}
              </div>
       )
}

ViewIngredient.propTypes = {
       removeIngredientFromProduct: PropTypes.func.isRequired,
}

export default connect(null, {
       removeIngredientFromProduct,
})(ViewIngredient)
const useStyles = makeStyles((theme) => ({
       root: {
              width: '100%',
       },
       heading: {
              fontSize: theme.typography.pxToRem(15),
              fontWeight: theme.typography.fontWeightRegular,
       },
}))
