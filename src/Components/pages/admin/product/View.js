import React, { useState, useEffect } from 'react'
import { Col, Row } from 'reactstrap'
import { Container } from '@material-ui/core'
import Assign from './AssignIngredient'
import { getAllIngredients } from '../../../../providers/actions/ingredient'
import { connect } from 'react-redux'
import ProductCardview from '../product/ProductCardview'
import ViewInregiendt from './ViewIngredient'
const View = ({ Products, getAllIngredients, ingredient }) => {
       useEffect(() => {
              getAllIngredients()
       }, [])
       return (
              <div>
                     <Container>
                            <Row xs='3'>
                                   {Products.map((prod) => (
                                          <Col xs='4' style={{ marginTop: '10px' }}>
                                                 <ProductCardview
                                                        product={prod}
                                                        Assign={Assign}
                                                        allIngredient={ingredient}
                                                        ViewIngredient={ViewInregiendt}
                                                 />
                                          </Col>
                                   ))}
                            </Row>
                     </Container>
              </div>
       )
}

const mapStateToProps = (state) => ({
       ingredient: state.ingredient.all,
})

export default connect(mapStateToProps, {
       getAllIngredients,
})(View)
