/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { createUnRelatedProduct } from '../../../../providers/actions/product'
import { TextField, Input, makeStyles, Container } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

import { InputGroup, Row, Label, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import _ from 'lodash'
import ImgView from '../Card/add/ImgView'
const Form = ({ createUnRelatedProduct, errors, action }) => {
       const classes = useStyles()
       const dispatch = useDispatch()
       const [Errors, setErrors] = useState({})
       const [ProductObject, setProductObject] = useState({
              name: '',
              description: '',
              price: '',
              image: '',
       })
       const [modal, setModal] = useState(false)

       const toggle = () => {
              setModal(!modal)
              setProductObject({
                     name: '',
                     description: '',
                     price: '',
                     image: '',
              })
       }

       const onChange = (e) => {
              setProductObject({ ...ProductObject, [e.target.name]: e.target.value })
       }
       const onSubmit = () => {
              var bodyFormData = new FormData()
              bodyFormData.set('name', ProductObject.name)
              bodyFormData.set('description', ProductObject.description)
              bodyFormData.set('price', ProductObject.price)
              bodyFormData.append('image', ProductObject.image)
              createUnRelatedProduct(bodyFormData)
       }
       useEffect(() => {
              if (errors) setErrors(errors)
       }, [errors])
       useEffect(() => {
              if (action === 'ADD_PRODUCT_SUCCESS') {
                     setModal(false)
                     setProductObject({
                            name: '',
                            description: '',
                            price: '',
                            image: '',
                     })

                     dispatch({
                            type: 'ADD_PRODUCT_CLEAR',
                     })
              }
       }, [action])
       return (
              <div className={classes.root}>
                     <Button onClick={(e) => toggle()} className='button-add-product'>
                            <Row>
                                   <Col>
                                          <div className='add-Product-text'>Create new Product</div>
                                   </Col>
                                   <Col xs='2'>
                                          <div className='add-Product-icon'>
                                                 <AddIcon />
                                          </div>
                                   </Col>
                            </Row>
                     </Button>
                     <Modal className='add-Product-modal' isOpen={modal} toggle={toggle}>
                            <ModalHeader toggle={toggle}>Add new Product </ModalHeader>
                            <ModalBody>
                                   <Container>
                                          <Row>
                                                 <Col xs='6'>
                                                        <InputGroup className='mb-4'>
                                                               <TextField
                                                                      id='outlined-helperText'
                                                                      label='Name'
                                                                      variant='outlined'
                                                                      name='name'
                                                                      onChange={(e) => onChange(e)}
                                                                      error={!_.isNil(Errors.name)}
                                                                      helperText={Errors.name}
                                                               />
                                                        </InputGroup>
                                                        <InputGroup className='mb-4'>
                                                               <TextField
                                                                      style={{ marginLeft: '1px' }}
                                                                      id='outlined-helperText'
                                                                      label='description'
                                                                      variant='outlined'
                                                                      name='description'
                                                                      onChange={(e) => onChange(e)}
                                                                      error={!_.isNil(Errors.description)}
                                                                      helperText={Errors.description}
                                                               />
                                                        </InputGroup>
                                                        <InputGroup className='mb-4'>
                                                               <TextField
                                                                      style={{ marginLeft: '1px' }}
                                                                      id='outlined-helperText'
                                                                      label='Price'
                                                                      variant='outlined'
                                                                      name='price'
                                                                      onChange={(e) => onChange(e)}
                                                                      error={!_.isNil(Errors.price)}
                                                                      helperText={Errors.price}
                                                               />
                                                        </InputGroup>

                                                        <InputGroup className='mb-4'>
                                                               <Row>
                                                                      {' '}
                                                                      <Label sm='5' size='sm' htmlFor='filePicker' id='upload-image-filial'>
                                                                             Choose image...
                                                                      </Label>
                                                               </Row>
                                                               <Row>
                                                                      {' '}
                                                                      <div style={{ color: 'red', marginTop: '10px', marginLeft: '27px' }}>
                                                                             {Errors.image}
                                                                      </div>
                                                               </Row>

                                                               <Input
                                                                      id='filePicker'
                                                                      style={{ display: 'none' }}
                                                                      type={'file'}
                                                                      name='image'
                                                                      onChange={(e) =>
                                                                             setProductObject({
                                                                                    ...ProductObject,
                                                                                    [e.target.name]: e.target.files[0],
                                                                             })
                                                                      }
                                                               />
                                                        </InputGroup>
                                                 </Col>
                                                 <Col>
                                                        <ImgView ImgObject={ProductObject} />{' '}
                                                 </Col>
                                          </Row>
                                   </Container>
                            </ModalBody>
                            <ModalFooter>
                                   <Button className='cancel-btn' onClick={toggle}>
                                          Cancel
                                   </Button>
                                   <Button className='save-btn' onClick={(e) => onSubmit()}>
                                          save
                                   </Button>
                            </ModalFooter>
                     </Modal>
              </div>
       )
}
Form.prototype = {
       createUnRelatedProduct: PropTypes.func.isRequired,
}
const StateProps = (state) => ({
       errors: state.errors,
       action: state.product.action,
       allProducts: state.product.allProducts,
})
export default connect(StateProps, {
       createUnRelatedProduct,
})(withRouter(Form))
const useStyles = makeStyles((theme) => ({
       root: {
              '& > *': {
                     margin: theme.spacing(1),
              },
       },
       extendedIcon: {
              marginRight: theme.spacing(1),
       },
}))
