/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Row, Col, CardFooter } from "reactstrap";
import { Container } from "@material-ui/core";
import View from "./action/View";
import outOfStock from "../../../../assets/img/brand/out_of_stock.png";
import { useDispatch } from "react-redux";
import { ADD } from "../../../../redux/actions/action";

import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const ProductCardview = ({
  product,
  Assign,
  allIngredient,
  ViewIngredient,
  category,
  fromCategory,
  cardID,
}) => {
  const classes = useStyles();
  const [ProductObject, setProductObject] = useState(product);
  useEffect(() => {
    setProductObject(product);
    onChangeStatus();
  }, [product]);
  const onChangeStatus = (e) => {
    if (product["ingredientObligatoire"].length === 0) {
      setProductObject({ ...ProductObject, status: "banned" });
    } else if (product) {
      product["ingredientObligatoire"].map((ing) => {
        if (ing.status === "unactive") {
          setProductObject({ ...ProductObject, status: "banned" });
        }
      });
    } else {
      setProductObject({ ...ProductObject, status: null });
    }
  };

  console.log("productObject : ", ProductObject);
  console.log("Product : ", product);

  //add to cart stuff

  // const dispatch = useDispatch();

  // const send = (e) => {
  //   console.log(e)

  //   dispatch(ADD(e))

  // }

  function addItemToCart(e) {
    var name = e.name;
    var price = e.price;
    var picture = e.image;
    var id = e._id;
    var qty = 1;
    var products = JSON.parse(localStorage.getItem("productsInCart") || "[]"); // get current objects
    var product = {
      id: id,
      name: name,
      price: price,
      qty: qty,
      picture: picture,
    };

    //increase quantity
    // const products = [{id: 1, name: 'Prod 1'}, {id: 2, name: 'Prod 2'}, {id: 3, name: 'Prod 3'}, {id: 4, name: 'Prod 4'}]
    // const cart = JSON.parse(localStorage.getItem('productsInCart')) || [];

    // const addToCart = (id) => {
    //     const check_index = cart.findIndex(item => item.id === id);
    //     if (check_index !== -1) {
    //       cart[check_index].quantity++;
    //       console.log("Quantity updated:", cart);
    //     } else {
    //       cart.push({...products.find(p => p.id === id), quantity: 1})
    //       console.log('The product has been added to cart:', cart);
    //     }
    // }

    // addToCart(4)
    //increase quantity

    const check_index = products.findIndex((item) => item.id === id);
    if (check_index !== -1) {
      products[check_index].qty++;
      console.log("Quantity updated:", products);
    } else {
      products.push(product);
      console.log("The product has been added to cart:", products);
    }

    //push new one
    // products.push(product);

    window.localStorage.setItem("productsInCart", JSON.stringify(products));
    window.dispatchEvent(new Event("storage"));
  }

  //add to cart stuff end

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    console.log("hello");
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div class="productContainer">
      <div
        className={"product-card-view"}
        id={ProductObject._id}
        key={ProductObject._id}
      >
        {ProductObject.status === "banned" ? (
          <Card
            className={classes.root}
            key={ProductObject._id}
            id="outOfStockProduct"
          >
            <CardHeader
              component={"span"}
              // title={product.name}
              id="productName"
            />
            <CardMedia
              className={`outOfStockProductImg ${classes.media}`}
              image={product.image}
              title={product.name}
              id="productImgContainer"
            />
            <span class="MuiTypography-root MuiCardHeader-title MuiTypography-h5 MuiTypography-displayBlock">
              {product.name}
            </span>
            <CardContent id="productDescription">
              <Typography
                variant="body2"
                color="textSecondary"
                component={"span"}
              >
                {product.description}
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <Container>
                <Row>
                  {/* <div className="productQtContainer"> */}
                  {/* <span id="productPrice">{product.price} €</span> */}
                  {/* <button
                      id="disabledBtn"
                      disabled
                      className="minusQtProduct"
                    >
                      -
                    </button>
                    <label className="qtProduct">1</label>
                    <button id="disabledBtn" disabled className="plusQtProduct">
                      +
                    </button> */}
                  {/* </div> */}
                </Row>

                <div id="addToCardContainer">
                  <button
                    id="disabledBtn"
                    disabled
                    className={`addToCartBtn DesktopTabletOnly`}
                    onClick={handleOpen}
                  >
                    &nbsp; &nbsp; &nbsp; {product.price} € &nbsp; &nbsp; &nbsp;
                    +
                  </button>
                  <button
                    className={`addToCartBtn MobileOnly`}
                    onClick={handleOpen}
                  >
                    &nbsp; {product.price} € &nbsp; +
                  </button>
                </div>
              </Container>
            </CardActions>
            {/* <CardFooter>
              {ProductObject.status === "banned" ? (
                <div style={{ color: "red" }}>Non Disponible</div>
              ) : (
                <div style={{ color: "green" }}>Disponible</div>
              )}{" "}
            </CardFooter> */}
          </Card>
        ) : (
          <Card className={classes.root} key={ProductObject._id}>
            <CardHeader
              component={"span"}
              id="productName"
              // title={product.name}
            />
            <CardMedia
              className={classes.media}
              image={product.image}
              title={product.name}
              id="productImgContainer"
            />
            <span class="MuiTypography-root MuiCardHeader-title MuiTypography-h5 MuiTypography-displayBlock">
              {product.name}
            </span>
            <CardContent id="productDescription">
              <Typography
                variant="body2"
                color="textSecondary"
                component={"span"}
              >
                {product.description}
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <Container>
                <Row>
                  {/* <div className="productQtContainer"> */}
                  {/* <span id="productPrice">{product.price} €</span> */}
                  {/* <button className="minusQtProduct">-</button>
                    <label className="qtProduct">1</label>
                    <button className="plusQtProduct">+</button> */}
                  {/* </div> */}
                </Row>

                <div id="addToCardContainer">
                  <button
                    className={`addToCartBtn DesktopTabletOnly`}
                    onClick={handleOpen}
                  >
                    &nbsp; &nbsp; &nbsp; {product.price} € &nbsp; &nbsp; &nbsp;
                    +
                  </button>
                  <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                      timeout: 500,
                    }}
                  >
                    <Fade in={open}>
                      <div
                        className={classes.paper}
                        id="ProductIngredientChooserContainer"
                      >
                        <img src={ProductObject.image}></img>
                        <h3>{ProductObject.name}</h3>
                        <h3>{ProductObject.price} €</h3>
                        <h2 id="transition-modal-title">Choisir Ingrédients</h2>

                        <button
                          className={`addToCartBtn`}
                          onClick={() => addItemToCart(ProductObject)}
                        >
                          Confirmer
                        </button>
                      </div>
                    </Fade>
                  </Modal>
                  <button
                    className={`addToCartBtn MobileOnly`}
                    onClick={handleOpen}
                  >
                    &nbsp; {product.price} € &nbsp; +
                  </button>
                </div>
              </Container>
            </CardActions>
            {/* <CardFooter>
              {ProductObject.status === "banned" ? (
                <div style={{ color: "red" }}>Non Disponible</div>
              ) : (
                <div style={{ color: "green" }}>Disponible</div>
              )}{" "}
            </CardFooter> */}
          </Card>
        )}
      </div>
    </div>
  );
};

ProductCardview.propTypes = {};

export default ProductCardview;
