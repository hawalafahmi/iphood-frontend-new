import React, { useState, useEffect, Fragment } from 'react'
import { MDBDataTableV5 } from 'mdbreact'
import './dataTable.css'
import 'font-awesome/css/font-awesome.min.css'
import Avatar from 'react-avatar'
import AdminModal from '../adminUpdate/AdminUpdate'
import { getAdminById } from '../../../../providers/actions/admin'
import { connect, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { Button } from '@material-ui/core'
const DataTable = ({ adminList, getAdminById, admin, actionUpdate }) => {
    const [ModalState, setModalState] = useState(false)
    const [Admin, setAdmin] = useState(null)
    const Dispatch = useDispatch()
    const data = {
        columns: [
            {
                label: '',
                field: 'image',
                width: 50,
            },
            {
                label: 'First name',
                field: 'firstName',
                width: 150,
                /* sort: 'dsc', */
                attributes: {
                    'aria-controls': 'DataTable',
                    'aria-label': 'firstName',
                },
            },
            {
                label: 'Last name',
                field: 'lastName',
                width: 150,
            },
            {
                label: 'Email',
                field: 'Email',
                width: 150,
            },

            {
                label: 'Restaurants',
                field: 'resto',
                /* sort: 'asc', */
                width: 150,
            },
            {
                label: 'Users',
                field: 'users',
                width: 250,
            },
            {
                /*   label: 'Actions', */
                field: 'actions',
                /* sort: 'asc', */
                width: 10,
            },
        ],
        rows: [],
    }
    useEffect(() => {
        if (admin) {
            setAdmin(admin)
            setModalState(true)
        }
    }, [admin])
    useEffect(() => {
        if (actionUpdate) {
            Dispatch({ type: 'ADMIN_UPDATE_CLEAR' })
            setModalState(false)
        }
    }, [Dispatch, actionUpdate])
    const openModale = (userID) => {
        getAdminById(userID)
    }

    const closeModal = () => {
        Dispatch({ type: 'ADMIN_GET_ID_CLEAR' })
        setModalState(false)
    }

    if (adminList && data.rows.length === 0)
        adminList.reverse().map((admin, index) =>
            data.rows.push({
                image: admin.restaurant[0] ? (
                    <img className='datatable-image-restaurant' alt='' src={admin.restaurant[0].image} />
                ) : (
                    <Avatar round='20px' size='50' name={admin.firstName} />
                ),
                Email: admin.email,
                firstName: admin.firstName,
                lastName: admin.lastName,
                resto: admin.allowedRestaurant,
                users: admin.allowedUsers,
                actions: (
                    <div className='update-admin-icon'>
                        <Button
                            variant='contained'
                            color='primary'
                            className='admin-details-button-modify'
                            onClick={(e) => {
                                openModale(admin._id)
                            }}
                        >
                            Modify
                        </Button>
                    </div>
                ),
            })
        )
    return (
        <Fragment>
            <MDBDataTableV5 hover pagesAmount={4} entries={5} data={data} pagingTop searchTop searchBottom={false} />
            <AdminModal ModalState={ModalState} openModale={openModale} closeModal={closeModal} Admin={Admin} />
        </Fragment>
    )
}
DataTable.propTypes = {
    getAdminById: PropTypes.func.isRequired,
}

const StateProps = (state) => ({
    list: state.admin.list,
    admin: state.admin.oneAdmin,
    actionUpdate: state.admin.action,
})
export default connect(StateProps, { getAdminById })(DataTable)
