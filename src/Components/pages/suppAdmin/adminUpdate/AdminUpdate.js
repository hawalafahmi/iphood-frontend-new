import React, { useEffect, useState } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import moment from 'moment'
import { connect, useDispatch } from 'react-redux'
import _ from 'lodash'
import PropTypes from 'prop-types'
import './updateModal.css'
import { Button } from '@material-ui/core'
import { updateAdminByID } from '../../../../providers/actions/admin'
const useStyles = makeStyles((theme) => ({
    root: {},
}))
const AdminUpdate = ({ Admin, ModalState, closeModal, updateAdminByID, errors }) => {
    const dispatch = useDispatch()
    const [ButtonsState, setButtonsState] = useState(true)
    const [Errors, setErrors] = useState({})
    const [User, setUser] = useState()
    useEffect(() => {
        setUser(Admin)
    }, [Admin])
    const classes = useStyles()
    const onChange = (e) => {
        setUser({ ...User, [e.target.name]: e.target.value })
    }
    useEffect(() => {
        if (_.isEqual(User, Admin)) {
            setButtonsState(true)
        } else {
            setButtonsState(false)
        }
    }, [User, Admin])
    useEffect(() => {
        setErrors(errors)
    }, [errors])
    const error = Errors
    const onSubmit = () => {
        dispatch({ type: 'CLEAR_ERRORS' })
        updateAdminByID(User._id, User)
    }
    const onCLose = () => {
        dispatch({ type: 'CLEAR_ERRORS' })

        closeModal()
    }
    return (
        <div className='modalUpdateUser'>
            {User ? (
                <Modal isOpen={ModalState} toggle={() => closeModal()}>
                    <ModalHeader toggle={() => closeModal()}>Admin Profile</ModalHeader>
                    <ModalBody>
                        {' '}
                        <form className={classes.root}>
                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    label='Registered at'
                                    disabled
                                    defaultValue={moment(User.createdAt).format('MM/DD/YYYY')}
                                />
                            </div>

                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    label='First name'
                                    name='firstName'
                                    error={!_.isNil(error.firstName)}
                                    helperText={error.firstName}
                                    defaultValue={User.firstName}
                                    onChange={(e) => onChange(e)}
                                />
                            </div>

                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    label='Last name'
                                    name='lastName'
                                    error={!_.isNil(error.lastName)}
                                    helperText={error.lastName}
                                    onChange={(e) => onChange(e)}
                                    defaultValue={User.lastName}
                                />
                            </div>

                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    label='Email'
                                    name='email'
                                    error={!_.isNil(error.email)}
                                    helperText={error.email}
                                    onChange={(e) => onChange(e)}
                                    defaultValue={User.email}
                                />
                            </div>

                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    onChange={(e) => onChange(e)}
                                    label='Restaurant'
                                    name='allowedRestaurant'
                                    error={!_.isNil(error.allowedRestaurant)}
                                    helperText={error.allowedRestaurant}
                                    defaultValue={User.allowedRestaurant}
                                />
                            </div>
                            <div>
                                <TextField
                                    className='update-admin-modal-input'
                                    id='standard-basic'
                                    onChange={(e) => onChange(e)}
                                    label='Users'
                                    name='allowedUsers'
                                    error={!_.isNil(error.allowedUsers)}
                                    helperText={error.allowedUsers}
                                    defaultValue={User.allowedUsers}
                                />
                            </div>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            onClick={() => onCLose()}
                            /*   disabled={ButtonsState} */
                            className='cancel-button'
                            variant='contained'
                            color='secondary'
                        >
                            Cancel{' '}
                        </Button>
                        <Button
                            disabled={ButtonsState}
                            onClick={() => onSubmit()}
                            className='accepted-button'
                            variant='contained'
                            color='secondary'
                        >
                            Update{' '}
                        </Button>
                    </ModalFooter>
                </Modal>
            ) : (
                <div></div>
            )}
        </div>
    )
}

AdminUpdate.propTypes = {
    updateAdminByID: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
}
const StateProps = (state) => ({
    errors: state.errors,
})
export default connect(StateProps, { updateAdminByID })(AdminUpdate)
