import React, { useState, useEffect } from 'react'
import { AppSwitch } from '@coreui/react'

import {
    Button,
    Col,
    Form,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row,
    FormGroup,
    Label,
} from 'reactstrap'
import PropTypes from 'prop-types'
import { connect, useDispatch } from 'react-redux'
import classnames from 'classnames'
import { register } from '../../../providers/actions/auth'
import { addNewRestaurant } from '../../../providers/actions/restaurant'
import './register.css'
const Register = ({
    register,
    errors,
    toggle,
    registerUser,
    registerAction,
    addNewRestaurant,
    restaurantRegisterAction,
}) => {
    const dispatch = useDispatch()
    const [Errors, setErrors] = useState({})
    const [RestObject, setRestObject] = useState({ name: '', image: File })
    const [registerData, setregisterData] = useState({
        firstName: '',
        lastName: '',
        password: '',
        confirmPassword: '',
        email: '',
        isAdmin: false,
        isSuperAdmin: false,
    })
    const onChange = (e) => {
        setregisterData({ ...registerData, [e.target.name]: e.target.value })
    }
    const onChangeSwitchers = (e) => {
        setregisterData({ ...registerData, [e.target.name]: e.target.checked })
    }

    const onSubmit = (e) => {
        e.preventDefault()
        register(registerData)
    }
    const onSubmitResto = (e) => {
        e.preventDefault()

        var bodyFormData = new FormData()
        bodyFormData.set('name', RestObject.name)
        bodyFormData.append('image', RestObject.image)

        addNewRestaurant(registerUser._id, bodyFormData)
    }
    useEffect(() => {
        if (errors) {
            setErrors(errors)
        }
    }, [errors])

    if (restaurantRegisterAction) {
        dispatch({ type: 'ADD_REST_CLEAR' })
        dispatch({ type: 'CLEAR_REGISTER' })
        toggle()
    }

    return (
        <div>
            {!registerAction ? (
                <Form onSubmit={(e) => onSubmit(e)}>
                    <InputGroup className='mb-3'>
                        <InputGroupAddon addonType='prepend'>
                            <InputGroupText>
                                <i className='icon-user'></i>
                            </InputGroupText>
                        </InputGroupAddon>
                        {/* FIRST name input  */}
                        <Input
                            type='text'
                            placeholder='First name'
                            name='firstName'
                            onChange={(e) => onChange(e)}
                            className={classnames('form-control form-control-lg', {
                                'is-invalid': Errors.firstName,
                            })}
                        />
                        {Errors.firstName && <div className='invalid-feedback'>{Errors.firstName}</div>}
                    </InputGroup>
                    <InputGroup className='mb-3'>
                        <InputGroupAddon addonType='prepend'>
                            <InputGroupText>
                                <i className='icon-user'></i>
                            </InputGroupText>
                        </InputGroupAddon>
                        {/* Last name input  */}
                        <Input
                            type='text'
                            placeholder='Last name'
                            name='lastName'
                            onChange={(e) => onChange(e)}
                            className={classnames('form-control form-control-lg', {
                                'is-invalid': Errors.lastName,
                            })}
                        />
                        {Errors.lastName && <div className='invalid-feedback'>{Errors.lastName}</div>}
                    </InputGroup>
                    <InputGroup className='mb-3'>
                        <InputGroupAddon addonType='prepend'>
                            <InputGroupText>@</InputGroupText>
                        </InputGroupAddon>
                        {/* Email name input  */}
                        <Input
                            type='text'
                            placeholder='Email'
                            name='email'
                            onChange={(e) => onChange(e)}
                            className={classnames('form-control form-control-lg', {
                                'is-invalid': Errors.email,
                            })}
                        />
                        {Errors.email && <div className='invalid-feedback'>{Errors.email}</div>}
                    </InputGroup>
                    <InputGroup className='mb-3'>
                        <InputGroupAddon addonType='prepend'>
                            <InputGroupText>
                                <i className='icon-lock'></i>
                            </InputGroupText>
                        </InputGroupAddon>
                        {/* password  input  */}
                        <Input
                            type='password'
                            placeholder='Password'
                            autoComplete='new-password'
                            name='password'
                            onChange={(e) => onChange(e)}
                            className={classnames('form-control form-control-lg', {
                                'is-invalid': Errors.password,
                            })}
                        />
                        {Errors.password && <div className='invalid-feedback'>{Errors.password}</div>}
                    </InputGroup>
                    <InputGroup className='mb-4'>
                        <InputGroupAddon addonType='prepend'>
                            <InputGroupText>
                                <i className='icon-lock'></i>
                            </InputGroupText>
                        </InputGroupAddon>
                        {/*confirm password input  */}
                        <Input
                            type='password'
                            placeholder='Repeat password'
                            autoComplete='new-password'
                            name='confirmPassword'
                            onChange={(e) => onChange(e)}
                            className={classnames('form-control form-control-lg', {
                                'is-invalid': Errors.confirmPassword,
                            })}
                        />
                        {Errors.confirmPassword && <div className='invalid-feedback'>{Errors.confirmPassword}</div>}
                    </InputGroup>
                    <InputGroup className='mb-4'>
                        <div className='switch-text'>Super Admin</div>

                        <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            name='isSuperAdmin'
                            onChange={(e) => onChangeSwitchers(e)}
                        />
                    </InputGroup>
                    <InputGroup className='mb-4'>
                        <div className='switch-text' style={{ marginRight: ' 39px' }}>
                            Admin
                        </div>
                        <AppSwitch
                            className={'mx-1'}
                            variant={'pill'}
                            color={'primary'}
                            name='isAdmin'
                            onChange={(e) => onChangeSwitchers(e)}
                        />
                    </InputGroup>
                    <Button className='accept-Button' block>
                        Create Account
                    </Button>
                </Form>
            ) : (
                <Form onSubmit={(e) => onSubmitResto(e)} className='form-horizontal'>
                    <FormGroup row>
                        <Label sm='5' size='sm' htmlFor='input-small'>
                            Restaurant name
                        </Label>
                        <Col sm='6'>
                            <Input
                                type='text'
                                name='name'
                                placeholder='name'
                                onChange={(e) =>
                                    setRestObject({
                                        ...RestObject,
                                        [e.target.name]: e.target.value,
                                    })
                                }
                                className={classnames('form-control form-control-lg', {
                                    'is-invalid': Errors.name,
                                })}
                            />
                            {Errors.name && <div className='invalid-feedback'>{Errors.name}</div>}
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label sm='5' size='sm' htmlFor='input-small'>
                            Restaurant image
                        </Label>

                        <Col sm='6'>
                            <Row>
                                <Col>
                                    {' '}
                                    <Label sm='5' size='sm' htmlFor='filePicker' id='upload-image-restaurant'>
                                        Choose File...
                                    </Label>
                                </Col>
                                <Col>
                                    {RestObject.image && RestObject.image.name !== 'File' ? (
                                        <Label>{RestObject.image.name}</Label>
                                    ) : null}
                                </Col>
                            </Row>
                            <Input
                                id='filePicker'
                                style={{ display: 'none' }}
                                type={'file'}
                                name='image'
                                onChange={(e) =>
                                    setRestObject({
                                        ...RestObject,
                                        [e.target.name]: e.target.files[0],
                                    })
                                }
                                className={classnames('form-control form-control-lg', {
                                    'is-invalid': Errors.image,
                                })}
                            />
                            {!(RestObject.image && RestObject.image.name !== 'File') && Errors.image && (
                                <div className='invalid-feedback'>{Errors.image}</div>
                            )}
                        </Col>
                    </FormGroup>
                    <InputGroup className='mb-4'></InputGroup>
                    <Button className='accept-Button' block>
                        Add restaurant
                    </Button>
                </Form>
            )}
        </div>
    )
}

Register.prototype = {
    register: PropTypes.func.isRequired,
    addNewRestaurant: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
}
const StateProps = (state) => ({
    errors: state.errors,
    registerAction: state.auth.register,
    registerUser: state.auth.newUser,
    restaurantRegisterAction: state.restaurant.action,
})
export default connect(StateProps, {
    register,
    addNewRestaurant,
})(Register)
