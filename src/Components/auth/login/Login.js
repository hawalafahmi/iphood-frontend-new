import React, { useState, useEffect } from 'react'
import './login.css'
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { login } from '../../../providers/actions/auth'
import { Redirect } from 'react-router-dom'

const Login = ({ login, errors, auth }) => {
     const [Errors, setErrors] = useState({})
     const [LoginData, setLoginData] = useState({
          password: '',
          email: '',
     })
     const onChange = (e) => {
          setLoginData({ ...LoginData, [e.target.name]: e.target.value })
     }
     const onSubmit = (e) => {
          e.preventDefault()
          login(LoginData)
     }

     useEffect(() => {
          if (errors) {
               setErrors(errors)
          }
     }, [errors])

     const error = Errors
     return localStorage.token ? (
          <Redirect to='/' />
     ) : (
          <div className='app flex-row align-items-center login-body'>
               <Container className='login-container'>
                    <Row className='justify-content-center'>
                         <Col md='8'>
                              <CardGroup className='login-card_Group'>
                                   <Card className='p-4 login-card'>
                                        <CardBody>
                                             <Form onSubmit={(e) => onSubmit(e)}>
                                                  <h1>Login</h1>
                                                  <p className='text-muted'>Sign In to your account</p>
                                                  <InputGroup className='mb-3'>
                                                       <InputGroupAddon addonType='prepend'>
                                                            <InputGroupText>
                                                                 <i className='icon-user'></i>
                                                            </InputGroupText>
                                                       </InputGroupAddon>
                                                       <Input
                                                            type='text'
                                                            placeholder='Username'
                                                            autoComplete='username'
                                                            name='email'
                                                            onChange={(e) => onChange(e)}
                                                            className={classnames('form-control form-control-lg', {
                                                                 'is-invalid': error.email,
                                                            })}
                                                       />
                                                       {error.email && <div className='invalid-feedback'>{error.email}</div>}
                                                  </InputGroup>
                                                  <InputGroup className='mb-4'>
                                                       <InputGroupAddon addonType='prepend'>
                                                            <InputGroupText>
                                                                 <i className='icon-lock'></i>
                                                            </InputGroupText>
                                                       </InputGroupAddon>
                                                       <Input
                                                            type='password'
                                                            placeholder='Password'
                                                            autoComplete='current-password'
                                                            name='password'
                                                            onChange={(e) => onChange(e)}
                                                            className={classnames('form-control form-control-lg', {
                                                                 'is-invalid': error.password,
                                                            })}
                                                       />
                                                       {error.password && <div className='invalid-feedback'>{error.password}</div>}
                                                  </InputGroup>
                                                  <Row>
                                                       <Col xs='6'>
                                                            <Button color='primary' className='px-4'>
                                                                 Login
                                                            </Button>
                                                       </Col>
                                                       <Col xs='6' className='text-right'>
                                                            {/*    <Button color='link' className='px-0'>
              Forgot password?
            </Button> */}
                                                       </Col>
                                                  </Row>
                                             </Form>
                                        </CardBody>
                                   </Card>
                              </CardGroup>
                         </Col>
                    </Row>
               </Container>
          </div>
     )
}

Login.prototype = {
     login: PropTypes.func.isRequired,
     errors: PropTypes.object.isRequired,
}
const StateProps = (state) => ({
     errors: state.errors,
     auth: state.auth,
})
export default connect(StateProps, {
     login,
})(Login)
