export default {
       items: [
              {
                     name: 'Admin dashboard',
                     url: '/',
                     /*   icon: 'icon-speedometer', */
              },

              {
                     name: 'Filials',
                     url: '/filial',
                     icon: 'icon-pencil',
                     children: [
                            {
                                   name: '  Filial list',
                                   url: '/filial/list',
                            },
                            {
                                   name: 'Add New Filial',
                                   url: '/filial/add',
                            },
                     ],
              },
              {
                     name: 'Cards',
                     url: '/card',
                     icon: 'icon-cursor',
                     children: [],
              },
              {
                     name: 'Product',
                     url: '/product',
                     icon: 'icon-cursor',
              },
              {
                     name: 'Ingredient',
                     url: '/ingredient',
                     icon: 'icon-cursor',
              },
       ],
}
