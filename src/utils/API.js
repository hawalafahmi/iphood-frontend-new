import axios from 'axios'
var baseURL
process.env.NODE_ENV !== 'development'
    ? (baseURL = process.env.REACT_APP_BASE_URL)
    : (baseURL = 'http://localhost:5200')

const API = axios.create({
    baseURL: baseURL + '/api',
    headers: {
        'Content-Type': 'application/json',
    },
})
API.interceptors.response.use(
    (res) => res,
    (err) => {
        if (err.response.data.error === 'Unauthorized') {
            /*  store.dispatch({ type: USER_LOGOUT }) */
        }
        return Promise.reject(err)
    }
)

export default API
