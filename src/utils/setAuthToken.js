import API from './API'

const setAuthToken = (token) => {
    if (token) {
        API.defaults.headers.common['Authorization'] = token
        localStorage.setItem('token', token)
    } else {
        delete API.defaults.headers.common['Authorization']
        localStorage.removeItem('token')
    }
}

export default setAuthToken
