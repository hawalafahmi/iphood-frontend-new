import React from 'react'

const routes = [
       {
              path: '/register',
              exact: true,
              name: 'Register',
              superAdmin: true,
              component: React.lazy(() => import('./Components/auth/register/Register')),
       },
       {
              path: '/users/list',
              exact: true,
              name: 'Admin list',
              superAdmin: true,
              component: React.lazy(() => import('./Components/pages/suppAdmin/UsersList')),
       },

       {
              path: '/filial/',
              exact: true,
              name: 'Filial ',
       },
       {
              path: '/filial/list',
              exact: true,
              isAdmin: true,

              name: 'Filial List',
              component: React.lazy(() => import('./Components/pages/admin/Filial/List/List')),
       },
       {
              path: '/filial/add',
              exact: true,
              isAdmin: true,
              name: 'Add new filial',
              component: React.lazy(() => import('./Components/pages/admin/Filial/add/Layout')),
       },

       {
              path: '/card',
              exact: true,
              isAdmin: true,
              name: 'Card',
       },
       {
              path: '/',
              exact: true,
              isAdmin: true,
              name: 'Product',
              component: React.lazy(() => import('./Components/pages/admin/product/Layout')),
       },
       {
              path: '/ingredient',
              exact: true,
              isAdmin: true,
              name: 'Ingredient',
              component: React.lazy(() => import('./Components/pages/admin/ingrediant/Layout')),
       },

       {
              path: '/404',
              exact: true,
              name: '404 not found',
              component: React.lazy(() => import('./Components/auth/404/404.js')),
       },
]

export default routes
